Tomb Raider Speedruns Resources
=========================================

This repository contains resources (autosplitters, patchers, etc.) useful for speedrunning classic Tomb Raider games.
[TRS Discord Server](https://discord.gg/KV6brta)

## See Also

* [TRS Version Database](https://gitlab.com/tombraiderspeedrunning/trs_version_database)
* [TRS Records](https://gitlab.com/tombraiderspeedrunning/trs_records)
