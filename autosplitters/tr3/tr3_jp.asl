/*
 * Tomb Raider 3 Japanese Version Autosplitter for Livesplit
 *
 * Copyright (C) DarenK 2018
 * Support: https://discord.gg/KV6brta Tomb Raider Speedruns (TRS) Discord Server
 *
 * Version 1.1
 * Up to date version available at https://gitlab.com/tombraiderspeedruns/trs_resources
 *
 * This autosplitter times whenever the IGT is running (without loads/FMV's/cutscenes).
 *
 * Instructions:
 * 1. Edit Layout --> (+) Control->Scriptable Autosplitter --> Browse to .asl
 * 2. Edit Layout --> Timer or Detailed Timer --> Timing Method: Game Time
 */

state("tomb3") {
	int level_number:		0x0C561C;
	byte fmv_flag:			0x233F5C;
	int igt_running_flag:	0x233F64;
	byte title_flag:		0x2A1B80;
}

start {
	return (current.level_number == 1 && current.igt_running_flag == 1);
}

reset {
	return (current.title_flag == 1 && old.title_flag == 0);
}

isLoading {
	if (current.igt_running_flag == 1) {
		return false;
	} else {
		return true;
	}
}

split {
	// Hardcoded the current Any% route
	// Check out tr3_route.ods to know how to modify the values

	// Jungle levels
	if (1 <= current.level_number && current.level_number <= 4
		&& current.level_number == timer.CurrentSplitIndex + 2) {
		return true;
	}

	// Nevada levels
	if (13 <= current.level_number && current.level_number <= 15 
		&& current.level_number == timer.CurrentSplitIndex + 10) {
		return true;
	}

	// London levels
	if (9 <= current.level_number && current.level_number <= 12 
		&& current.level_number == timer.CurrentSplitIndex + 3) {
		return true;
	}

	// South Pacific levels
	if (5 <= current.level_number && current.level_number <= 8
		&& current.level_number == timer.CurrentSplitIndex - 5) {
		return true;
	}

	// Antarctica levels
	if (16 <= current.level_number && current.level_number <= 19
		&& current.level_number == timer.CurrentSplitIndex + 2) {
		return true;
	}
	
	if (current.level_number == 19) { // split on last fmv
		return (current.fmv_flag == 1);
	}
}
