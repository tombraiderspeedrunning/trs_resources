--Tomb Raider Script for PSX versions of game created by lapogne36 and Temple Of Horus (contains additional informations for PC versions)
--Created for resolution: 1920x1080
--console.log( joypad.getimmediate()

--Coordinates:
--X = west-east
--Y = up-down
--Z = north-south

local HASH=gameinfo.getromhash()

--Basic informations:
local POINTER_LARA = 0		--Pointer to Lara's data location |TR1 PC: |TR2 PC: 0x5207BC |TR3 PC: 0x6D62A4 |
local OFFSET_FIX = 0		--Fix for pointers
local OFFSET_POSITION = 0		--Coordinates location in Lara's data
local OFFSET_SPEEDH = 0		--Speed location in Lara's data
local OFFSET_SPEEDV = 0	 --Fall speed location in Lara's data
local OFFSET_ANGLE = 0		--Angle location in Lara's data
local OFFSET_HEALTH = 0		--Health location in Lara's data
local OFFSET_ROOM = 0	 --Current room location in Lara's data
local OFFSET_CUR_ACT = 0	--currentAction location in Lara's data
local OFFSET_ANIM_FRA = 0	--animFrame location in Lara's data

local NUMROOM = 0	   --Number of rooms in level location in the RAM
local OFFSET_TIMER	= 0		--Timer location in the RAM 
local OFFSET_TIMER1 = 0	 --Time of first level in the RAM
local TIMERLEV = 0	  --Number of levels in the game to calculate total time in TR1, TR2, TR3 (0 also counts as 1 level)
local FLIPMAP = 0	   --Flipmap location in the RAM
local ORLEV = 0		 --Ordinal number of current level location in RAM

--Entities:
local OFFSET_ENTITY = 0		--Pointer to entity's data - Lara must draws weapon on entity (equal to 0 if none)
local OFFSET_ENTITY_ID = 0		--ID location in entity's data
local OFFSET_ENTITY_HP = 0		--HP location in entity's data
local ENTITY_LIST = {}	--Entities list based on the entities ID
local LIVENT = 0		--Living entities location in the RAM (counter shows maximum 5 living entities)
local NUMOFENT = 0	  --Number of entities in level location in the RAM
local OFFSET_FIRST_ENTITY = 0  --Pointer to data of first entity in level location
local OFFSET_FIRST_ENTITY_ID = 0   --ID location of first entity in data of first entity
local OFFSET_FIRST_ENTITY_X = 0	--X position location of first entity in data of first entity
local OFFSET_FIRST_ENTITY_Y = 0	--Y position location of first entity in data of first entity
local OFFSET_FIRST_ENTITY_Z = 0	--Z position location of first entity in data of first entity
local OFFSET_FIRST_ENTITY_ROOM = 0 --Room location of first entity in data of first entity
local SIZOFENT = 0	  --Size of data of one entity in the RAM 

--QWOP - change value to 8 while Lara running if you want to do QWOP (2 bytes)
local OFFSET_QWOP = 0	 --QWOP location in Lara's data |PC: TR2: Base + 0x46 |PC: TR3: Base + 0x82 |

--Camera:
local CAMERAX = 0	   --X position of camera
local CAMERAY = 0	   --Y position of camera
local CAMERAZ = 0	   --Z position of camera
local CAMERAROOM = 0	--Camera room (helps find out from which rooms you can activate trigger with camera - useful for TR4, TR5)
local CAM_ANGLE = 0	 --Camera angle

local CAMUPFUN = 0	  --Camera update function

--Other:
local OFFSET_ACT = 0	--Pointer on some entity (IDK)
local OFFSET_TRIGGER = 0  --Pointer to currently activated trigger data or to data of triggers which are close to Lara (TR3 PC version - 0x6E29A8) (4 bytes)
local SPRINT = 0		--If value is 0x0, Lara has not sprint function in the game (TR1, TR2). If value is another, Lara has sprint function in the game (0x3 for TR3, 0x4 for TR4, 0x5 for TR5).

while true do

BASE_OLD=0
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------TR1---------------------------------------------------------------------------------------------------------
-- Tomb Raider (1996)
-- First release date: November 1996
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='5651035B' then	--TR1 (SLUS_001.52) (USA)

--Basic informations:
	POINTER_LARA = 0x89FAC
	OFFSET_FIX = 0x80000000
	OFFSET_POSITION = 0x30
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x3E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0		  --needs add
	OFFSET_ANIM_FRA = 0		  --needs add
	
	NUMROOM = 0			 --needs add
	OFFSET_TIMER = 0x928F8
	OFFSET_TIMER1 = 0		   --needs add
	TIMERLEV = 0			--needs add
	FLIPMAP = 0			 --needs add
	ORLEV = 0			   --needs add

--Entities:
	OFFSET_ENTITY = 0x1DE050
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Including Lara skin
'1: Lara pistols animation';		 --Animations used for pistol shooting
'2: Lara shotgun animation';		 --As above, for shotgun
'3: Lara magnums animation';		 --As above, for magnums
'4: Lara uzis animation';			--As above, for uzis
'5: Lara misc';					  --Various additional anims and models used across the game (Home suit, wounded Lara, Gold Lara etc.)
'6: Doppelganger';				   --Mirrors Lara behaviour
'7: Wolf';
'8: Bear';
'9: Bat';
'10: Crocodile (on land)';
'11: Crocodile (in water)';
'12: Lion (male)';
'13: Lion (female)';
'14: Panther';
'15: Gorilla';
'16: Rat (on land)';
'17: Rat (in water)';
'18: T-Rex';
'19: Raptor';
'20: Mutant';						--Flying Atlantean mutant
'21: Mutant spawn point 1';		  --Requires model ID #20 present. Spawns same ID, but with on-land and shooting behaviour.
'22: Mutant spawn point 2';		  --As above, but without shooting behaviour.
'23: Centaur mutant';
'24: Mummy';
'25: DinoWarrior';				   --Leftover from beta versions, unused in final game
'26: Fish';						  --Leftover from beta versions, unused in final game
'27: Larson';						--Full name: Larson Conway
'28: Pierre';						--Full name: Pierre Dupont
'29: Skateboard';					--Linked to entity ID# 30 at runtime
'30: Skateboard Kid';				--Full name: Jerome Johnson
'31: The Cowboy';
'32: Kold';						  --Full name: Kin Kade, previously known as Mr. T
'33: Winged Natla';
'34: Torso Boss';
'35: Falling block';				 --Hard collision above floor level, trigger should be in sector to activate
'36: Swinging blade';
'37: Teeth spikes';
'38: Rolling ball';
'39: Dart';						  --Projectile, dynamically generated by entity ID #40
'40: Dart emitter';
'41: Lifting door';				  --Acts in reverse with activation mask set on start-up
'42: Slamming doors';
'43: Sword of Damocles';
'44: Thor’s hammer’s handle';
'45: Thor’s hammer’s block';		 --Linked to handle at runtime
'46: Thor’s lightning ball';		 --Only first mesh is rendered, others are nullmesh pointers for lighting strikes
'47: Barricade';					 --Used to block final door in LEVEL4.PHD
'48: Pushable block 1';			  --Adds value of (4 x 256 = 1024) to floordata height below
'49: Pushable block 2';			  --As above
'50: Pushable block 3';			  --As above
'51: Pushable block 4';			  --As above
'52: Moving block';				  --Adds value of (8 x 256 = 2048) to floordata height below
'53: Falling ceiling';
'54: Sword of Damocles';			 --Duplicate of entity ID #43
'55: Wall switch';
'56: Underwater switch';
'57: Door 1';						--When closed, puts WALL (0x81) property to sector(s) behind, recursively including sector in neighbour room, if collisional portal is present
'58: Door 2';						--As above
'59: Door 3';						--As above
'60: Door 4';						--As above
'61: Door 5';						--As above
'62: Door 6';						--As above
'63: Door 7';						--As above
'64: Door 8';						--As above
'65: Trapdoor 1';					--When closed, blocks portal below, if exists
'66: Trapdoor 2';					--As above
'67: ';
'68: Bridge (flat)';				 --Trigger should be in sector to be standable
'69: Bridge tilt 1';				 --Trigger should be in sector to be standable
'70: Bridge tilt 2';				 --Trigger should be in sector to be standable
'71: Passport (opening)';
'72: Compass';
'73: Lara’s Home photo';
'74: Animating 1';				   --Used as cogs in LEVEL3A.PHD
'75: Animating 2';				   --As above
'76: Animating 3';				   --As above
'77: Cutscene actor 1';
'78: Cutscene actor 2';
'79: Cutscene actor 3';
'80: Cutscene actor 4';
'81: Passport (closed)';
'82: Map';						   --Present only in beta versions
'83: Savegame crystal';			  --Unused in PC versions
'84: Pistols';
'85: Shotgun';
'86: Magnums';
'87: Uzis';
'88: Pistol ammo';				   --No actual effect on inventory
'89: Shotgun ammo';
'90: Magnum ammo';
'91: Uzi ammo';
'92: Explosive';					 --Present only in beta versions
'93: Small medipack';				--Restores 1/2 of Lara HP
'94: Large medipack';				--Restores Lara HP to maximum
'95: Sunglasses';
'96: Cassette player';
'97: Direction keys';
'98: Flashlight';
'99: Pistols';
'100: Shotgun';
'101: Magnums';
'102: Uzis';
'103: Pistol ammo';
'104: Shotgun ammo';
'105: Magnum ammo';
'106: Uzi ammo';
'107: Explosive';					--Present only in beta versions		
'108: Small medipack';
'109: Large medipack';
'110: Puzzle 1';
'111: Puzzle 2';
'112: Puzzle 3';
'113: Puzzle 4';
'114: Puzzle 1';
'115: Puzzle 2';
'116: Puzzle 3';
'117: Puzzle 4';
'118: Puzzle hole 1';				--Swaps to entity ID #122 when entity ID #114 is used
'119: Puzzle hole 2';				--Swaps to entity ID #123 when entity ID #115 is used
'120: Puzzle hole 3';				--Swaps to entity ID #124 when entity ID #116 is used
'121: Puzzle hole 4';				--Swaps to entity ID #125 when entity ID #117 is used
'122: Puzzle done 1';
'123: Puzzle done 2';
'124: Puzzle done 3';
'125: Puzzle done 4';
'126: Lead bar';					 --Used against entity ID #128 to transform into entity ID #110
'127: Lead bar';
'128: Midas gold touch';			 --When Lara is standing in same sector, turns her to gold. When entity ID #127 applied in radius of ~1/2 sector, perform transformation to entity ID #114
'129: Key 1';
'130: Key 2';
'131: Key 3';
'132: Key 4';
'133: Key 1';
'134: Key 2';
'135: Key 3';
'136: Key 4';
'137: Keyhole 1';					--Used with entity ID #129
'138: Keyhole 2';					--Used with entity ID #130
'139: Keyhole 3';					--Used with entity ID #131
'140: Keyhole 4';					--Used with entity ID #132
'141: Complete Scion';
'142: Complete Scion';
'143: Scion piece';				  --Special way (next to, not below) and animation for pick-up
'144: Scion Piece';
'145: Scion (shootable)';			--Used in LEVEL10C.PHD
'146: Scion';						--Used in LEVEL10B.PHD
'147: Scion holder';
'148: ';
'149: ';
'150: Scion piece';
'151: Explosion';
'152: ';
'153: Water ripples';				--Emitted by entity ID #170 and Lara going underwater
'154: ';
'155: Bubbles';					  --Emitted by Lara underwater
'156: Bubbles';
'157: ';
'158: Blood';
'159: ';
'160: Smoke';
'161: Centaur statue';
'162: Natla’s Mines shack';		  --Produces no actual collision, which is done via flipmaps
'163: Mutant egg (small)';		   --Spawns different mutant types, according to own’s initial activation mask
'164: Ricochet';
'165: Sparkles';					 --Used for Midas golden effect
'166: Gunflare';					 --Rendered when shooting
'167: ';
'168: ';
'169: Camera target';
'170: Waterfall mist';			   --Generates water ripples below
'171: ';
'172: Mutant bullet';				--Projectile, dynamically generated by mutant
'173: Mutant grenade';			   --As above
'174: ';
'175: ';
'176: Lava particles';
'177: Lava particle emitter';		--Produces sound and bouncing sprite bubbles
'178: Flame';
'179: Flame emitter';				--Generates flame sprite sequence, sets Lara on fire in radius of ~1/2 sector
'180: Flowing Atlantean lava';	   --When activated, moves until stopped by wall or slope
'181: Mutant egg (big)';			 --For Torso Boss
'182: Motorboat';					--Produces no actual collision, which is done via flipmaps
'183: Earthquake';				   --Shakes camera and plays several rumble sound FXs
'184: ';
'185: ';
'186: ';
'187: ';
'188: ';
'189: Lara’s ponytail';			  --Never finished and never used in game, present in LEVEL3A.PHD
'190: Font graphics';
'191: Plant 1';
'192: Plant 2';
'193: Plant 3';
'194: Plant 4';
'195: Plant 5';
'196: ';
'197: ';
'198: ';
'199: ';
'200: Bag 1';
'201: ';
'202: ';
'203: ';
'204: Bag 2';
'205: ';
'206: ';
'207: Gunflare';
'208: ';
'209: ';
'210: ';
'211: ';
'212: Rock 1';
'213: Rock 2';
'214: Rock 3';
'215: Bag 3';
'216: Debris 1';
'217: Debris 2';
'218: ';
'219: ';
'220: ';
'221: ';
'222: ';
'223: ';
'224: ';
'225: ';
'226: ';
'227: ';
'228: ';
'229: ';
'230: ';
'231: Debris 3';
'232: ';
'233: Inca mummy';
'234: ';
'235: ';
'236: Debris 4';
'237: Debris 5';
'238: Debris 6';
'239: Debris 7';
	}					  --needs add
	LIVENT = 0			 --needs add
	NUMOFENT = 0x088C24	--0x089FC4
	OFFSET_FIRST_ENTITY = 0x089BFC
	OFFSET_FIRST_ENTITY_ID = 0xC
	OFFSET_FIRST_ENTITY_X = 0		 --needs add
	OFFSET_FIRST_ENTITY_Y = 0		 --needs add
	OFFSET_FIRST_ENTITY_Z = 0		 --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0	   --needs add
	SIZOFENT = 0x48

--QWOP:
	OFFSET_QWOP = 0x44
	
--Camera:
	CAMERAX = 0			--needs add
	CAMERAY = 0			--needs add
	CAMERAZ = 0			--needs add
	CAMERAROOM = 0		 --needs add	   
	CAM_ANGLE = 0		  --needs add

	CAMUPFUN = 0		   --needs add
	
--Other:
	OFFSET_ACT = 0		 --needs add
	OFFSET_TRIGGER = 0	   --needs add
	SPRINT = 0x0
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='6FE9AAC5' then	--TR1 (SLPS_006.17) (JAP)

--Basic informations:
	POINTER_LARA = 0x08A09C
	OFFSET_FIX = 0x80000000
	OFFSET_POSITION = 0x30
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x3E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0		 --needs add
	OFFSET_ANIM_FRA = 0		 --needs add
	
	NUMROOM = 0			--needs add
	OFFSET_TIMER = 0x0929E8
	OFFSET_TIMER1 = 0		  --needs add
	TIMERLEV = 0		   --needs add
	FLIPMAP = 0			--needs add
	ORLEV = 0			  --needs add

--Entities:
	OFFSET_ENTITY = 0x1DE140
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Including Lara skin
'1: Lara pistols animation';		 --Animations used for pistol shooting
'2: Lara shotgun animation';		 --As above, for shotgun
'3: Lara magnums animation';		 --As above, for magnums
'4: Lara uzis animation';			--As above, for uzis
'5: Lara misc';					  --Various additional anims and models used across the game (Home suit, wounded Lara, Gold Lara etc.)
'6: Doppelganger';				   --Mirrors Lara behaviour
'7: Wolf';
'8: Bear';
'9: Bat';
'10: Crocodile (on land)';
'11: Crocodile (in water)';
'12: Lion (male)';
'13: Lion (female)';
'14: Panther';
'15: Gorilla';
'16: Rat (on land)';
'17: Rat (in water)';
'18: T-Rex';
'19: Raptor';
'20: Mutant';						--Flying Atlantean mutant
'21: Mutant spawn point 1';		  --Requires model ID #20 present. Spawns same ID, but with on-land and shooting behaviour.
'22: Mutant spawn point 2';		  --As above, but without shooting behaviour.
'23: Centaur mutant';
'24: Mummy';
'25: DinoWarrior';				   --Leftover from beta versions, unused in final game
'26: Fish';						  --Leftover from beta versions, unused in final game
'27: Larson';						--Full name: Larson Conway
'28: Pierre';						--Full name: Pierre Dupont
'29: Skateboard';					--Linked to entity ID# 30 at runtime
'30: Skateboard Kid';				--Full name: Jerome Johnson
'31: The Cowboy';
'32: Kold';						  --Full name: Kin Kade, previously known as Mr. T
'33: Winged Natla';
'34: Torso Boss';
'35: Falling block';				 --Hard collision above floor level, trigger should be in sector to activate
'36: Swinging blade';
'37: Teeth spikes';
'38: Rolling ball';
'39: Dart';						  --Projectile, dynamically generated by entity ID #40
'40: Dart emitter';
'41: Lifting door';				  --Acts in reverse with activation mask set on start-up
'42: Slamming doors';
'43: Sword of Damocles';
'44: Thor’s hammer’s handle';
'45: Thor’s hammer’s block';		 --Linked to handle at runtime
'46: Thor’s lightning ball';		 --Only first mesh is rendered, others are nullmesh pointers for lighting strikes
'47: Barricade';					 --Used to block final door in LEVEL4.PHD
'48: Pushable block 1';			  --Adds value of (4 x 256 = 1024) to floordata height below
'49: Pushable block 2';			  --As above
'50: Pushable block 3';			  --As above
'51: Pushable block 4';			  --As above
'52: Moving block';				  --Adds value of (8 x 256 = 2048) to floordata height below
'53: Falling ceiling';
'54: Sword of Damocles';			 --Duplicate of entity ID #43
'55: Wall switch';
'56: Underwater switch';
'57: Door 1';						--When closed, puts WALL (0x81) property to sector(s) behind, recursively including sector in neighbour room, if collisional portal is present
'58: Door 2';						--As above
'59: Door 3';						--As above
'60: Door 4';						--As above
'61: Door 5';						--As above
'62: Door 6';						--As above
'63: Door 7';						--As above
'64: Door 8';						--As above
'65: Trapdoor 1';					--When closed, blocks portal below, if exists
'66: Trapdoor 2';					--As above
'67: ';
'68: Bridge (flat)';				 --Trigger should be in sector to be standable
'69: Bridge tilt 1';				 --Trigger should be in sector to be standable
'70: Bridge tilt 2';				 --Trigger should be in sector to be standable
'71: Passport (opening)';
'72: Compass';
'73: Lara’s Home photo';
'74: Animating 1';				   --Used as cogs in LEVEL3A.PHD
'75: Animating 2';				   --As above
'76: Animating 3';				   --As above
'77: Cutscene actor 1';
'78: Cutscene actor 2';
'79: Cutscene actor 3';
'80: Cutscene actor 4';
'81: Passport (closed)';
'82: Map';						   --Present only in beta versions
'83: Savegame crystal';			  --Unused in PC versions
'84: Pistols';
'85: Shotgun';
'86: Magnums';
'87: Uzis';
'88: Pistol ammo';				   --No actual effect on inventory
'89: Shotgun ammo';
'90: Magnum ammo';
'91: Uzi ammo';
'92: Explosive';					 --Present only in beta versions
'93: Small medipack';				--Restores 1/2 of Lara HP
'94: Large medipack';				--Restores Lara HP to maximum
'95: Sunglasses';
'96: Cassette player';
'97: Direction keys';
'98: Flashlight';
'99: Pistols';
'100: Shotgun';
'101: Magnums';
'102: Uzis';
'103: Pistol ammo';
'104: Shotgun ammo';
'105: Magnum ammo';
'106: Uzi ammo';
'107: Explosive';					--Present only in beta versions		
'108: Small medipack';
'109: Large medipack';
'110: Puzzle 1';
'111: Puzzle 2';
'112: Puzzle 3';
'113: Puzzle 4';
'114: Puzzle 1';
'115: Puzzle 2';
'116: Puzzle 3';
'117: Puzzle 4';
'118: Puzzle hole 1';				--Swaps to entity ID #122 when entity ID #114 is used
'119: Puzzle hole 2';				--Swaps to entity ID #123 when entity ID #115 is used
'120: Puzzle hole 3';				--Swaps to entity ID #124 when entity ID #116 is used
'121: Puzzle hole 4';				--Swaps to entity ID #125 when entity ID #117 is used
'122: Puzzle done 1';
'123: Puzzle done 2';
'124: Puzzle done 3';
'125: Puzzle done 4';
'126: Lead bar';					 --Used against entity ID #128 to transform into entity ID #110
'127: Lead bar';
'128: Midas gold touch';			 --When Lara is standing in same sector, turns her to gold. When entity ID #127 applied in radius of ~1/2 sector, perform transformation to entity ID #114
'129: Key 1';
'130: Key 2';
'131: Key 3';
'132: Key 4';
'133: Key 1';
'134: Key 2';
'135: Key 3';
'136: Key 4';
'137: Keyhole 1';					--Used with entity ID #129
'138: Keyhole 2';					--Used with entity ID #130
'139: Keyhole 3';					--Used with entity ID #131
'140: Keyhole 4';					--Used with entity ID #132
'141: Complete Scion';
'142: Complete Scion';
'143: Scion piece';				  --Special way (next to, not below) and animation for pick-up
'144: Scion Piece';
'145: Scion (shootable)';			--Used in LEVEL10C.PHD
'146: Scion';						--Used in LEVEL10B.PHD
'147: Scion holder';
'148: ';
'149: ';
'150: Scion piece';
'151: Explosion';
'152: ';
'153: Water ripples';				--Emitted by entity ID #170 and Lara going underwater
'154: ';
'155: Bubbles';					  --Emitted by Lara underwater
'156: Bubbles';
'157: ';
'158: Blood';
'159: ';
'160: Smoke';
'161: Centaur statue';
'162: Natla’s Mines shack';		  --Produces no actual collision, which is done via flipmaps
'163: Mutant egg (small)';		   --Spawns different mutant types, according to own’s initial activation mask
'164: Ricochet';
'165: Sparkles';					 --Used for Midas golden effect
'166: Gunflare';					 --Rendered when shooting
'167: ';
'168: ';
'169: Camera target';
'170: Waterfall mist';			   --Generates water ripples below
'171: ';
'172: Mutant bullet';				--Projectile, dynamically generated by mutant
'173: Mutant grenade';			   --As above
'174: ';
'175: ';
'176: Lava particles';
'177: Lava particle emitter';		--Produces sound and bouncing sprite bubbles
'178: Flame';
'179: Flame emitter';				--Generates flame sprite sequence, sets Lara on fire in radius of ~1/2 sector
'180: Flowing Atlantean lava';	   --When activated, moves until stopped by wall or slope
'181: Mutant egg (big)';			 --For Torso Boss
'182: Motorboat';					--Produces no actual collision, which is done via flipmaps
'183: Earthquake';				   --Shakes camera and plays several rumble sound FXs
'184: ';
'185: ';
'186: ';
'187: ';
'188: ';
'189: Lara’s ponytail';			  --Never finished and never used in game, present in LEVEL3A.PHD
'190: Font graphics';
'191: Plant 1';
'192: Plant 2';
'193: Plant 3';
'194: Plant 4';
'195: Plant 5';
'196: ';
'197: ';
'198: ';
'199: ';
'200: Bag 1';
'201: ';
'202: ';
'203: ';
'204: Bag 2';
'205: ';
'206: ';
'207: Gunflare';
'208: ';
'209: ';
'210: ';
'211: ';
'212: Rock 1';
'213: Rock 2';
'214: Rock 3';
'215: Bag 3';
'216: Debris 1';
'217: Debris 2';
'218: ';
'219: ';
'220: ';
'221: ';
'222: ';
'223: ';
'224: ';
'225: ';
'226: ';
'227: ';
'228: ';
'229: ';
'230: ';
'231: Debris 3';
'232: ';
'233: Inca mummy';
'234: ';
'235: ';
'236: Debris 4';
'237: Debris 5';
'238: Debris 6';
'239: Debris 7';
	}					  --needs add
	LIVENT = 0			 --needs add
	NUMOFENT = 0		   --needs add
	OFFSET_FIRST_ENTITY = 0	   --needs add
	OFFSET_FIRST_ENTITY_ID = 0		--needs add
	OFFSET_FIRST_ENTITY_X = 0		 --needs add
	OFFSET_FIRST_ENTITY_Y = 0		 --needs add
	OFFSET_FIRST_ENTITY_Z = 0		 --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0	   --needs add
	SIZOFENT = 0		   --needs add

--QWOP:
	OFFSET_QWOP = 0x44

--Camera:
	CAMERAX = 0			--needs add
	CAMERAY = 0			--needs add
	CAMERAZ = 0			--needs add
	CAMERAROOM = 0		 --needs add
	CAM_ANGLE = 0		  --needs add
	
	CAMUPFUN = 0		   --needs add

--Other:
	OFFSET_ACT = 0		 --needs add
	OFFSET_TRIGGER = 0	   --needs add
	SPRINT = 0x0
end
--------------------------------------------------------------------------------------------------TR2------------------------------------------------------------------------------------------------------
-- Tomb Raider II (1997)
-- Tomb Raider II: Starring Lara Croft
-- First release date: November 1997
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='B089D427' or HASH =='C2CA9687' then	--TR2 (SLUS_004.37) (USA)

--Basic informations:
	POINTER_LARA = 0x8C6B8
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x34
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x42
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0		 --needs add
	OFFSET_ANIM_FRA = 0		 --needs add
	
	NUMROOM = 0			--needs add
	OFFSET_TIMER = 0x0DE670
	OFFSET_TIMER1 = 0		  --needs add
	TIMERLEV = 0		   --needs add
	FLIPMAP = 0			--needs add
	ORLEV = 0			  --needs add

--Entities:
	OFFSET_ENTITY = 0x8C568
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22 
	ENTITY_LIST={
'0: Lara';						   --Including Lara skin
'1: Lara pistols animation';		 --Same action as in TR1
'2: Lara’s ponytail';				--Hardcoded to attach to Lara’s head
'3: Lara shotgun animation';		 --Same action as in TR1
'4: Lara automags animation';		--As above, for automags
'5: Lara uzis animation';			--As above, for uzis
'6: Lara M16 animation';			 --As above, for M16
'7: Lara grenade gun animation';	 --As above, for grenade gun
'8: Lara harpoon gun animation';	 --As above, for harpoon gun
'9: Lara flare animation';		   --As above, for flare
'10: Lara snowmobile animation';	 --Used with snowmobile vehicles
'11: Lara boat animation';		   --Used with boat vehicles
'12: Lara misc animations';		  --Various additional anims used across the game (Talion gong anim, custom die anims)
'13: Red snowmobile';				--Vehicle; goes faster than black one, but can’t shoot
'14: Boat';						  --Vehicle
'15: Doberman';
'16: Masked goon 1';
'17: Masked goon 2';				 --Borrows animations from entity ID #16
'18: Masked goon 3';				 --As above
'19: Knifethrower';
'20: Shotgun goon';
'21: Rat';
'22: Dragon (front)';
'23: Dragon (back)';				 --Attaches to entity ID #22 at runtime
'24: Gondola';					   --Shatterable by boat vehicle
'25: Shark';
'26: Yellow moray eel';
'27: Black moray eel';
'28: Barracuda';
'29: Scuba diver';
'30: Gunman 1';
'31: Gunman 2';
'32: Stick-wielding goon 1';
'33: Stick-wielding goon 2';		 --Can’t climb
'34: Flamethrower goon';
'35: ';
'36: Spider';
'37: Giant spider';
'38: Crow';
'39: Tiger / Snow leopard';
'40: Marco Bartoli';				 --Not real NPC, laying still in Dragon’s Lair
'41: Xian Guard w/spear';
'42: Xian Guard w/spear statue';	 --Transforms to entity #41 on activation
'43: Xian Guard w/sword';
'44: Xian Guard w/sword statue';	 --Transforms to entity #43 on activation
'45: Yeti';
'46: Bird monster (guards Talion)';
'47: Eagle';
'48: Mercenary 1';
'49: Mercenary 2';				   --Black ski mask, gray jacket
'50: Mercenary 3';				   --Black ski mask, brown jacket
'51: Black snowmobile';			  --Vehicle; goes slower than red one, but can shoot
'52: Mercenary snowmobile driver';
'53: Monk with long stick';
'54: Monk with knife-end stick';
'55: Falling block';
'56: ';
'57: Loose boards';				  --Same action as entity ID #55
'58: Swinging sandbag / Spiky ball';
'59: Teeth spikes / Glass shards';
'60: Rolling ball';
'61: Disc';						  --Acts same way as dart in TR1
'62: Discgun';					   --Acts same way as dart emitter in TR1
'63: Drawbridge';
'64: Slamming door';
'65: Elevator';					  --Trigger should be in sector to be standable
'66: Minisub';
'67: Pushable block 1';			  --Same action as in TR1
'68: Pushable block 2';			  --As above
'69: Pushable block 3';			  --As above
'70: Pushable block 4';			  --As above
'71: Lava bowl';
'72: Breakable window 1';			--Must be shot to break
'73: Breakable window 2';			--Must be jumped through to break
'74: ';
'75: ';
'76: Airplane propeller';
'77: Power saw';
'78: Overhead pulley hook';
'79: Falling ceiling / Sandbag';
'80: Rolling spindle';
'81: Wall-mounted knife blade';
'82: Statue with knife blade';
'83: Multiple boulders / snowballs';
'84: Detachable icicles';
'85: Spiky wall';
'86: Bounce pad';					--Bounces Lara up
'87: Spiky ceiling';
'88: Tibetan bell';				  --Activates when shot
'89: ';
'90: ';
'91: Snowmobile belt';			   --Two meshes are swapped at runtime according to current snowmobile state
'92: Wheel knob';
'93: Small wall switch';
'94: Underwater propeller';
'95: Air fan';
'96: Swinging box / spiky ball';
'97: Cutscene actor 1';
'98: Cutscene actor 2';
'99: Cutscene actor 3';
'100: UI frame';					 --Used to create UI elements
'101: Rolling storage drums';
'102: Zipline handle';			   --When used, slides until there’s an obstacle ~1.5 sectors ahead; position is reset on every triggering
'103: Push-button switch';
'104: Wall switch';
'105: Underwater switch';
'106: Door 1';					   --Same action as in TR1
'107: Door 2';					   --As above
'108: Door 3';					   --As above
'109: Door 4';					   --As above
'110: Door 5';					   --As above
'111: Lifting door 1';			   --Same action as in TR1
'112: Lifting door 2';			   --As above
'113: Lifting door 3';			   --As above
'114: Trapdoor 1';				   --Same action as in TR1
'115: Trapdoor 2';				   --As above
'116: Trapdoor 3';				   --As above
'117: Bridge flat';				  --Same action as in TR1
'118: Bridge tilt 1';				--Same action as in TR1
'119: Bridge tilt 2';				--Same action as in TR1
'120: Secret 1';					 --Jade dragon
'121: Secret 2';					 --Silver dragon
'122: Lara’s home photo';
'123: Cutscene actor 4';
'124: Cutscene actor 5';
'125: Cutscene actor 6';
'126: Cutscene actor 7';
'127: Cutscene actor 8';
'128: Cutscene actor 9';
'129: Cutscene actor 10';
'130: Cutscene actor 11';
'131: ';
'132: ';
'133: Secret 3';					 --Gold dragon
'134: Map';
'135: Pistols';
'136: Shotgun';
'137: Automags';
'138: Uzi';
'139: Harpoon gun';
'140: M16';
'141: Grenade gun';
'142: Pistol ammo';				  --No actual effect on inventory
'143: Shotgun ammo';
'144: Auto-pistol ammo';
'145: Uzi ammo';
'146: Harpoon gun ammo';
'147: M16 ammo';
'148: Grenadegun ammo';
'149: Small medipack';
'150: Large medipack';
'151: Flares';
'152: Flare';						--Spawns when Lara throws flare away, re-pickupable
'153: Sunglasses';
'154: CD player';
'155: Direction keys';
'156: ';
'157: Pistols';
'158: Shotgun';
'159: Automags';
'160: Uzi';
'161: Harpoon gun';
'162: M16';
'163: Grenade gun';
'164: Pistol ammo';
'165: Shotgun ammo';
'166: Automag ammo';
'167: Uzi ammo';
'168: Harpoon gun ammo';
'169: M16 ammo';
'170: Grenadegun ammo';
'171: Small medipack';
'172: Large medipack';
'173: Flares';
'174: Puzzle 1';
'175: Puzzle 2';
'176: Puzzle 3';
'177: Puzzle 4';
'178: Puzzle 1';
'179: Puzzle 2';
'180: Puzzle 3';
'181: Puzzle 4';
'182: Puzzle hole 1';				--Swaps to entity ID #186 when entity ID #178 is used
'183: Puzzle hole 2';				--Swaps to entity ID #187 when entity ID #179 is used
'184: Puzzle hole 3';				--Swaps to entity ID #188 when entity ID #180 is used
'185: Puzzle hole 4';				--Swaps to entity ID #189 when entity ID #181 is used
'186: Puzzle done 1';
'187: Puzzle done 2';
'188: Puzzle done 3';
'189: Puzzle done 4';
'190: Secret 1';
'191: Secret 2';
'192: Secret 3';
'193: Key 1';
'194: Key 2';
'195: Key 3';
'196: Key 4';
'197: Key 1';
'198: Key 2';
'199: Key 3';
'200: Key 4';
'201: Keyhole 1';					--Used with entity ID #193
'202: Keyhole 2';					--Used with entity ID #194
'203: Keyhole 3';					--Used with entity ID #195
'204: Keyhole 4';					--Used with entity ID #196
'205: Quest item 1';
'206: Quest item 2';				 --Talion
'207: Quest item 1';
'208: Quest item 2';
'209: Dragon explosion effect 1';
'210: Dragon explosion effect 2';
'211: Dragon explosion effect 3';
'212: Alarm';						--Plays looped sound ID #332
'213: Dripping water';			   --Randomly plays sound ID #329
'214: T-Rex';
'215: Singing birds';				--Randomly plays sound ID #316
'216: Bartoli’s Hideout clock';
'217: Placeholder';				  --Purpose unknown
'218: Dragon bones (front)';		 --Transformed from entity ID #22 when dragon dies
'219: Dragon bones (back)';		  --Transformed from entity ID #23 when dragon dies
'220: Extra fire';				   --Used in Ice Palace
'221: ';
'222: Aquatic Mine (Venice)';
'223: Menu background';
'224: Gray disk';					--Purpose unknown
'225: Gong stick';				   --Contains animation for gong stick itself, Lara anim is placed in ID #12
'226: Gong';						 --Acts same way as ID #202, i.e. entity ID #194 can be used
'227: Detonator box';
'228: Helicopter (Diving Area)';	 --Flies upwards and away
'229: Explosion';					--Same action as in TR1
'230: Water ripples';				--As above
'231: Bubbles';					  --As above
'232: ';
'233: Blood';						--As above (229)
'234: ';
'235: Flare sparkles';			   --Used on activated flare, animation is done via randomized rotation
'236: Glow';						 --Overlay for M16 gunflare mesh
'237: ';
'238: Ricochet';					 --Same action as in TR1
'239: ';
'240: Gunflare';					 --Same action as in TR1, except M16
'241: M16 gunflare';				 --Used when shooting M16 only
'242: ';
'243: Camera target';				--Same action as in TR1
'244: Waterfall mist';			   --As above
'245: Harpoon';
'246: ';
'247: Placeholder';				  --Purpose unknown
'248: Grenade (single)';			 --Projectile, generated when shooting grenadegun
'249: Harpoon (flying)';			 --Projectile, generated when shooting harpoon gun
'250: Lava particles';			   --Same action as in TR1
'251: Lava / air particle emitter';  --As above
'252: Flame';						--As above
'253: Flame emitter';				--As above
'254: Skybox';					   --Rendered when any room with outside flag set is visible
'255: Font graphics';
'256: Monk';
'257: Door bell';					--Plays sound ID #334 once
'258: Alarm bell';				   --Plays looped sound ID #335
'259: Helicopter';				   --Travels around 30 sectors, then deactivates itself
'260: Winston';
'261: ';
'262: Lara cutscene placement';	  --Used in last level to engage shower cutscene
'263: Shotgun animation';			--Used in end game cutscene (shower)
'264: Dragon explosion emitter';	 --Activates when Lara at a close range, produces animation sequence using entity models ID #209-211, then spawns entity ID #22 at the end
	}					  --needs add
	LIVENT = 0			 --needs add
	NUMOFENT = 0		   --needs add
	OFFSET_FIRST_ENTITY = 0	   --needs add
	OFFSET_FIRST_ENTITY_ID = 0		--needs add
	OFFSET_FIRST_ENTITY_X = 0		 --needs add
	OFFSET_FIRST_ENTITY_Y = 0		 --needs add
	OFFSET_FIRST_ENTITY_Z = 0		 --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0	   --needs add
	SIZOFENT = 0		   --needs add

--QWOP:
	OFFSET_QWOP = 0x48

--Camera:
	CAMERAX = 0			--needs add
	CAMERAY = 0			--needs add
	CAMERAZ = 0			--needs add
	CAMERAROOM = 0		 --needs add
	CAM_ANGLE = 0		  --needs add
	
	CAMUPFUN = 0		   --needs add

--Other:
	OFFSET_ACT = 0x08B542
	OFFSET_TRIGGER = 0	   --needs add
	SPRINT = 0x0
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='7AF102CA' then	--TR2 (SLES_007.19) (FR)

--Basic informations:
	POINTER_LARA = 0x08C828
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x34
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x42
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0		 --needs add
	OFFSET_ANIM_FRA = 0		 --needs add
	
	NUMROOM = 0			--needs add
	OFFSET_TIMER = 0x0DE7E0
	OFFSET_TIMER1 = 0		  --needs add
	TIMERLEV = 0		   --needs add
	FLIPMAP = 0			--needs add
	ORLEV = 0			  --needs add

--Entities:
	OFFSET_ENTITY = 0x08C6D8
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Including Lara skin
'1: Lara pistols animation';		 --Same action as in TR1
'2: Lara’s ponytail';				--Hardcoded to attach to Lara’s head
'3: Lara shotgun animation';		 --Same action as in TR1
'4: Lara automags animation';		--As above, for automags
'5: Lara uzis animation';			--As above, for uzis
'6: Lara M16 animation';			 --As above, for M16
'7: Lara grenade gun animation';	 --As above, for grenade gun
'8: Lara harpoon gun animation';	 --As above, for harpoon gun
'9: Lara flare animation';		   --As above, for flare
'10: Lara snowmobile animation';	 --Used with snowmobile vehicles
'11: Lara boat animation';		   --Used with boat vehicles
'12: Lara misc animations';		  --Various additional anims used across the game (Talion gong anim, custom die anims)
'13: Red snowmobile';				--Vehicle; goes faster than black one, but can’t shoot
'14: Boat';						  --Vehicle
'15: Doberman';
'16: Masked goon 1';
'17: Masked goon 2';				 --Borrows animations from entity ID #16
'18: Masked goon 3';				 --As above
'19: Knifethrower';
'20: Shotgun goon';
'21: Rat';
'22: Dragon (front)';
'23: Dragon (back)';				 --Attaches to entity ID #22 at runtime
'24: Gondola';					   --Shatterable by boat vehicle
'25: Shark';
'26: Yellow moray eel';
'27: Black moray eel';
'28: Barracuda';
'29: Scuba diver';
'30: Gunman 1';
'31: Gunman 2';
'32: Stick-wielding goon 1';
'33: Stick-wielding goon 2';		 --Can’t climb
'34: Flamethrower goon';
'35: ';
'36: Spider';
'37: Giant spider';
'38: Crow';
'39: Tiger / Snow leopard';
'40: Marco Bartoli';				 --Not real NPC, laying still in Dragon’s Lair
'41: Xian Guard w/spear';
'42: Xian Guard w/spear statue';	 --Transforms to entity #41 on activation
'43: Xian Guard w/sword';
'44: Xian Guard w/sword statue';	 --Transforms to entity #43 on activation
'45: Yeti';
'46: Bird monster (guards Talion)';
'47: Eagle';
'48: Mercenary 1';
'49: Mercenary 2';				   --Black ski mask, gray jacket
'50: Mercenary 3';				   --Black ski mask, brown jacket
'51: Black snowmobile';			  --Vehicle; goes slower than red one, but can shoot
'52: Mercenary snowmobile driver';
'53: Monk with long stick';
'54: Monk with knife-end stick';
'55: Falling block';
'56: ';
'57: Loose boards';				  --Same action as entity ID #55
'58: Swinging sandbag / Spiky ball';
'59: Teeth spikes / Glass shards';
'60: Rolling ball';
'61: Disc';						  --Acts same way as dart in TR1
'62: Discgun';					   --Acts same way as dart emitter in TR1
'63: Drawbridge';
'64: Slamming door';
'65: Elevator';					  --Trigger should be in sector to be standable
'66: Minisub';
'67: Pushable block 1';			  --Same action as in TR1
'68: Pushable block 2';			  --As above
'69: Pushable block 3';			  --As above
'70: Pushable block 4';			  --As above
'71: Lava bowl';
'72: Breakable window 1';			--Must be shot to break
'73: Breakable window 2';			--Must be jumped through to break
'74: ';
'75: ';
'76: Airplane propeller';
'77: Power saw';
'78: Overhead pulley hook';
'79: Falling ceiling / Sandbag';
'80: Rolling spindle';
'81: Wall-mounted knife blade';
'82: Statue with knife blade';
'83: Multiple boulders / snowballs';
'84: Detachable icicles';
'85: Spiky wall';
'86: Bounce pad';					--Bounces Lara up
'87: Spiky ceiling';
'88: Tibetan bell';				  --Activates when shot
'89: ';
'90: ';
'91: Snowmobile belt';			   --Two meshes are swapped at runtime according to current snowmobile state
'92: Wheel knob';
'93: Small wall switch';
'94: Underwater propeller';
'95: Air fan';
'96: Swinging box / spiky ball';
'97: Cutscene actor 1';
'98: Cutscene actor 2';
'99: Cutscene actor 3';
'100: UI frame';					 --Used to create UI elements
'101: Rolling storage drums';
'102: Zipline handle';			   --When used, slides until there’s an obstacle ~1.5 sectors ahead; position is reset on every triggering
'103: Push-button switch';
'104: Wall switch';
'105: Underwater switch';
'106: Door 1';					   --Same action as in TR1
'107: Door 2';					   --As above
'108: Door 3';					   --As above
'109: Door 4';					   --As above
'110: Door 5';					   --As above
'111: Lifting door 1';			   --Same action as in TR1
'112: Lifting door 2';			   --As above
'113: Lifting door 3';			   --As above
'114: Trapdoor 1';				   --Same action as in TR1
'115: Trapdoor 2';				   --As above
'116: Trapdoor 3';				   --As above
'117: Bridge flat';				  --Same action as in TR1
'118: Bridge tilt 1';				--Same action as in TR1
'119: Bridge tilt 2';				--Same action as in TR1
'120: Secret 1';					 --Jade dragon
'121: Secret 2';					 --Silver dragon
'122: Lara’s home photo';
'123: Cutscene actor 4';
'124: Cutscene actor 5';
'125: Cutscene actor 6';
'126: Cutscene actor 7';
'127: Cutscene actor 8';
'128: Cutscene actor 9';
'129: Cutscene actor 10';
'130: Cutscene actor 11';
'131: ';
'132: ';
'133: Secret 3';					 --Gold dragon
'134: Map';
'135: Pistols';
'136: Shotgun';
'137: Automags';
'138: Uzi';
'139: Harpoon gun';
'140: M16';
'141: Grenade gun';
'142: Pistol ammo';				  --No actual effect on inventory
'143: Shotgun ammo';
'144: Auto-pistol ammo';
'145: Uzi ammo';
'146: Harpoon gun ammo';
'147: M16 ammo';
'148: Grenadegun ammo';
'149: Small medipack';
'150: Large medipack';
'151: Flares';
'152: Flare';						--Spawns when Lara throws flare away, re-pickupable
'153: Sunglasses';
'154: CD player';
'155: Direction keys';
'156: ';
'157: Pistols';
'158: Shotgun';
'159: Automags';
'160: Uzi';
'161: Harpoon gun';
'162: M16';
'163: Grenade gun';
'164: Pistol ammo';
'165: Shotgun ammo';
'166: Automag ammo';
'167: Uzi ammo';
'168: Harpoon gun ammo';
'169: M16 ammo';
'170: Grenadegun ammo';
'171: Small medipack';
'172: Large medipack';
'173: Flares';
'174: Puzzle 1';
'175: Puzzle 2';
'176: Puzzle 3';
'177: Puzzle 4';
'178: Puzzle 1';
'179: Puzzle 2';
'180: Puzzle 3';
'181: Puzzle 4';
'182: Puzzle hole 1';				--Swaps to entity ID #186 when entity ID #178 is used
'183: Puzzle hole 2';				--Swaps to entity ID #187 when entity ID #179 is used
'184: Puzzle hole 3';				--Swaps to entity ID #188 when entity ID #180 is used
'185: Puzzle hole 4';				--Swaps to entity ID #189 when entity ID #181 is used
'186: Puzzle done 1';
'187: Puzzle done 2';
'188: Puzzle done 3';
'189: Puzzle done 4';
'190: Secret 1';
'191: Secret 2';
'192: Secret 3';
'193: Key 1';
'194: Key 2';
'195: Key 3';
'196: Key 4';
'197: Key 1';
'198: Key 2';
'199: Key 3';
'200: Key 4';
'201: Keyhole 1';					--Used with entity ID #193
'202: Keyhole 2';					--Used with entity ID #194
'203: Keyhole 3';					--Used with entity ID #195
'204: Keyhole 4';					--Used with entity ID #196
'205: Quest item 1';
'206: Quest item 2';				 --Talion
'207: Quest item 1';
'208: Quest item 2';
'209: Dragon explosion effect 1';
'210: Dragon explosion effect 2';
'211: Dragon explosion effect 3';
'212: Alarm';						--Plays looped sound ID #332
'213: Dripping water';			   --Randomly plays sound ID #329
'214: T-Rex';
'215: Singing birds';				--Randomly plays sound ID #316
'216: Bartoli’s Hideout clock';
'217: Placeholder';				  --Purpose unknown
'218: Dragon bones (front)';		 --Transformed from entity ID #22 when dragon dies
'219: Dragon bones (back)';		  --Transformed from entity ID #23 when dragon dies
'220: Extra fire';				   --Used in Ice Palace
'221: ';
'222: Aquatic Mine (Venice)';
'223: Menu background';
'224: Gray disk';					--Purpose unknown
'225: Gong stick';				   --Contains animation for gong stick itself, Lara anim is placed in ID #12
'226: Gong';						 --Acts same way as ID #202, i.e. entity ID #194 can be used
'227: Detonator box';
'228: Helicopter (Diving Area)';	 --Flies upwards and away
'229: Explosion';					--Same action as in TR1
'230: Water ripples';				--As above
'231: Bubbles';					  --As above
'232: ';
'233: Blood';						--As above (229)
'234: ';
'235: Flare sparkles';			   --Used on activated flare, animation is done via randomized rotation
'236: Glow';						 --Overlay for M16 gunflare mesh
'237: ';
'238: Ricochet';					 --Same action as in TR1
'239: ';
'240: Gunflare';					 --Same action as in TR1, except M16
'241: M16 gunflare';				 --Used when shooting M16 only
'242: ';
'243: Camera target';				--Same action as in TR1
'244: Waterfall mist';			   --As above
'245: Harpoon';
'246: ';
'247: Placeholder';				  --Purpose unknown
'248: Grenade (single)';			 --Projectile, generated when shooting grenadegun
'249: Harpoon (flying)';			 --Projectile, generated when shooting harpoon gun
'250: Lava particles';			   --Same action as in TR1
'251: Lava / air particle emitter';  --As above
'252: Flame';						--As above
'253: Flame emitter';				--As above
'254: Skybox';					   --Rendered when any room with outside flag set is visible
'255: Font graphics';
'256: Monk';
'257: Door bell';					--Plays sound ID #334 once
'258: Alarm bell';				   --Plays looped sound ID #335
'259: Helicopter';				   --Travels around 30 sectors, then deactivates itself
'260: Winston';
'261: ';
'262: Lara cutscene placement';	  --Used in last level to engage shower cutscene
'263: Shotgun animation';			--Used in end game cutscene (shower)
'264: Dragon explosion emitter';	 --Activates when Lara at a close range, produces animation sequence using entity models ID #209-211, then spawns entity ID #22 at the end
	}					  --needs add
	LIVENT = 0x0897A4
	NUMOFENT = 0		   --needs add
	OFFSET_FIRST_ENTITY = 0	   --needs add
	OFFSET_FIRST_ENTITY_ID = 0		--needs add
	OFFSET_FIRST_ENTITY_X = 0		 --needs add
	OFFSET_FIRST_ENTITY_Y = 0		 --needs add
	OFFSET_FIRST_ENTITY_Z = 0		 --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0	   --needs add
	SIZOFENT = 0		   --needs add

--QWOP:
	OFFSET_QWOP = 0x48

--Camera:
	CAMERAX = 0			--needs add
	CAMERAY = 0			--needs add
	CAMERAZ = 0			--needs add
	CAMERAROOM = 0		 --needs add
	CAM_ANGLE = 0		  --needs add
	
	CAMUPFUN = 0		   --needs add

--Other:
	OFFSET_ACT = 0x08B6CA
	OFFSET_TRIGGER = 0	   --needs add
	SPRINT = 0x0
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='078B4703' then	--TR2 (SLES_007.18) (EU)

--Basic informations:
	POINTER_LARA = 0x08C828
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x34
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x42
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0		 --needs add
	OFFSET_ANIM_FRA = 0		 --needs add
	
	NUMROOM = 0			--needs add
	OFFSET_TIMER = 0x0DE7E0
	OFFSET_TIMER1 = 0		  --needs add
	TIMERLEV = 0		   --needs add
	FLIPMAP = 0			--needs add
	ORLEV = 0			  --needs add

--Entities:
	OFFSET_ENTITY = 0x08C6D8
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Including Lara skin
'1: Lara pistols animation';		 --Same action as in TR1
'2: Lara’s ponytail';				--Hardcoded to attach to Lara’s head
'3: Lara shotgun animation';		 --Same action as in TR1
'4: Lara automags animation';		--As above, for automags
'5: Lara uzis animation';			--As above, for uzis
'6: Lara M16 animation';			 --As above, for M16
'7: Lara grenade gun animation';	 --As above, for grenade gun
'8: Lara harpoon gun animation';	 --As above, for harpoon gun
'9: Lara flare animation';		   --As above, for flare
'10: Lara snowmobile animation';	 --Used with snowmobile vehicles
'11: Lara boat animation';		   --Used with boat vehicles
'12: Lara misc animations';		  --Various additional anims used across the game (Talion gong anim, custom die anims)
'13: Red snowmobile';				--Vehicle; goes faster than black one, but can’t shoot
'14: Boat';						  --Vehicle
'15: Doberman';
'16: Masked goon 1';
'17: Masked goon 2';				 --Borrows animations from entity ID #16
'18: Masked goon 3';				 --As above
'19: Knifethrower';
'20: Shotgun goon';
'21: Rat';
'22: Dragon (front)';
'23: Dragon (back)';				 --Attaches to entity ID #22 at runtime
'24: Gondola';					   --Shatterable by boat vehicle
'25: Shark';
'26: Yellow moray eel';
'27: Black moray eel';
'28: Barracuda';
'29: Scuba diver';
'30: Gunman 1';
'31: Gunman 2';
'32: Stick-wielding goon 1';
'33: Stick-wielding goon 2';		 --Can’t climb
'34: Flamethrower goon';
'35: ';
'36: Spider';
'37: Giant spider';
'38: Crow';
'39: Tiger / Snow leopard';
'40: Marco Bartoli';				 --Not real NPC, laying still in Dragon’s Lair
'41: Xian Guard w/spear';
'42: Xian Guard w/spear statue';	 --Transforms to entity #41 on activation
'43: Xian Guard w/sword';
'44: Xian Guard w/sword statue';	 --Transforms to entity #43 on activation
'45: Yeti';
'46: Bird monster (guards Talion)';
'47: Eagle';
'48: Mercenary 1';
'49: Mercenary 2';				   --Black ski mask, gray jacket
'50: Mercenary 3';				   --Black ski mask, brown jacket
'51: Black snowmobile';			  --Vehicle; goes slower than red one, but can shoot
'52: Mercenary snowmobile driver';
'53: Monk with long stick';
'54: Monk with knife-end stick';
'55: Falling block';
'56: ';
'57: Loose boards';				  --Same action as entity ID #55
'58: Swinging sandbag / Spiky ball';
'59: Teeth spikes / Glass shards';
'60: Rolling ball';
'61: Disc';						  --Acts same way as dart in TR1
'62: Discgun';					   --Acts same way as dart emitter in TR1
'63: Drawbridge';
'64: Slamming door';
'65: Elevator';					  --Trigger should be in sector to be standable
'66: Minisub';
'67: Pushable block 1';			  --Same action as in TR1
'68: Pushable block 2';			  --As above
'69: Pushable block 3';			  --As above
'70: Pushable block 4';			  --As above
'71: Lava bowl';
'72: Breakable window 1';			--Must be shot to break
'73: Breakable window 2';			--Must be jumped through to break
'74: ';
'75: ';
'76: Airplane propeller';
'77: Power saw';
'78: Overhead pulley hook';
'79: Falling ceiling / Sandbag';
'80: Rolling spindle';
'81: Wall-mounted knife blade';
'82: Statue with knife blade';
'83: Multiple boulders / snowballs';
'84: Detachable icicles';
'85: Spiky wall';
'86: Bounce pad';					--Bounces Lara up
'87: Spiky ceiling';
'88: Tibetan bell';				  --Activates when shot
'89: ';
'90: ';
'91: Snowmobile belt';			   --Two meshes are swapped at runtime according to current snowmobile state
'92: Wheel knob';
'93: Small wall switch';
'94: Underwater propeller';
'95: Air fan';
'96: Swinging box / spiky ball';
'97: Cutscene actor 1';
'98: Cutscene actor 2';
'99: Cutscene actor 3';
'100: UI frame';					 --Used to create UI elements
'101: Rolling storage drums';
'102: Zipline handle';			   --When used, slides until there’s an obstacle ~1.5 sectors ahead; position is reset on every triggering
'103: Push-button switch';
'104: Wall switch';
'105: Underwater switch';
'106: Door 1';					   --Same action as in TR1
'107: Door 2';					   --As above
'108: Door 3';					   --As above
'109: Door 4';					   --As above
'110: Door 5';					   --As above
'111: Lifting door 1';			   --Same action as in TR1
'112: Lifting door 2';			   --As above
'113: Lifting door 3';			   --As above
'114: Trapdoor 1';				   --Same action as in TR1
'115: Trapdoor 2';				   --As above
'116: Trapdoor 3';				   --As above
'117: Bridge flat';				  --Same action as in TR1
'118: Bridge tilt 1';				--Same action as in TR1
'119: Bridge tilt 2';				--Same action as in TR1
'120: Secret 1';					 --Jade dragon
'121: Secret 2';					 --Silver dragon
'122: Lara’s home photo';
'123: Cutscene actor 4';
'124: Cutscene actor 5';
'125: Cutscene actor 6';
'126: Cutscene actor 7';
'127: Cutscene actor 8';
'128: Cutscene actor 9';
'129: Cutscene actor 10';
'130: Cutscene actor 11';
'131: ';
'132: ';
'133: Secret 3';					 --Gold dragon
'134: Map';
'135: Pistols';
'136: Shotgun';
'137: Automags';
'138: Uzi';
'139: Harpoon gun';
'140: M16';
'141: Grenade gun';
'142: Pistol ammo';				  --No actual effect on inventory
'143: Shotgun ammo';
'144: Auto-pistol ammo';
'145: Uzi ammo';
'146: Harpoon gun ammo';
'147: M16 ammo';
'148: Grenadegun ammo';
'149: Small medipack';
'150: Large medipack';
'151: Flares';
'152: Flare';						--Spawns when Lara throws flare away, re-pickupable
'153: Sunglasses';
'154: CD player';
'155: Direction keys';
'156: ';
'157: Pistols';
'158: Shotgun';
'159: Automags';
'160: Uzi';
'161: Harpoon gun';
'162: M16';
'163: Grenade gun';
'164: Pistol ammo';
'165: Shotgun ammo';
'166: Automag ammo';
'167: Uzi ammo';
'168: Harpoon gun ammo';
'169: M16 ammo';
'170: Grenadegun ammo';
'171: Small medipack';
'172: Large medipack';
'173: Flares';
'174: Puzzle 1';
'175: Puzzle 2';
'176: Puzzle 3';
'177: Puzzle 4';
'178: Puzzle 1';
'179: Puzzle 2';
'180: Puzzle 3';
'181: Puzzle 4';
'182: Puzzle hole 1';				--Swaps to entity ID #186 when entity ID #178 is used
'183: Puzzle hole 2';				--Swaps to entity ID #187 when entity ID #179 is used
'184: Puzzle hole 3';				--Swaps to entity ID #188 when entity ID #180 is used
'185: Puzzle hole 4';				--Swaps to entity ID #189 when entity ID #181 is used
'186: Puzzle done 1';
'187: Puzzle done 2';
'188: Puzzle done 3';
'189: Puzzle done 4';
'190: Secret 1';
'191: Secret 2';
'192: Secret 3';
'193: Key 1';
'194: Key 2';
'195: Key 3';
'196: Key 4';
'197: Key 1';
'198: Key 2';
'199: Key 3';
'200: Key 4';
'201: Keyhole 1';					--Used with entity ID #193
'202: Keyhole 2';					--Used with entity ID #194
'203: Keyhole 3';					--Used with entity ID #195
'204: Keyhole 4';					--Used with entity ID #196
'205: Quest item 1';
'206: Quest item 2';				 --Talion
'207: Quest item 1';
'208: Quest item 2';
'209: Dragon explosion effect 1';
'210: Dragon explosion effect 2';
'211: Dragon explosion effect 3';
'212: Alarm';						--Plays looped sound ID #332
'213: Dripping water';			   --Randomly plays sound ID #329
'214: T-Rex';
'215: Singing birds';				--Randomly plays sound ID #316
'216: Bartoli’s Hideout clock';
'217: Placeholder';				  --Purpose unknown
'218: Dragon bones (front)';		 --Transformed from entity ID #22 when dragon dies
'219: Dragon bones (back)';		  --Transformed from entity ID #23 when dragon dies
'220: Extra fire';				   --Used in Ice Palace
'221: ';
'222: Aquatic Mine (Venice)';
'223: Menu background';
'224: Gray disk';					--Purpose unknown
'225: Gong stick';				   --Contains animation for gong stick itself, Lara anim is placed in ID #12
'226: Gong';						 --Acts same way as ID #202, i.e. entity ID #194 can be used
'227: Detonator box';
'228: Helicopter (Diving Area)';	 --Flies upwards and away
'229: Explosion';					--Same action as in TR1
'230: Water ripples';				--As above
'231: Bubbles';					  --As above
'232: ';
'233: Blood';						--As above (229)
'234: ';
'235: Flare sparkles';			   --Used on activated flare, animation is done via randomized rotation
'236: Glow';						 --Overlay for M16 gunflare mesh
'237: ';
'238: Ricochet';					 --Same action as in TR1
'239: ';
'240: Gunflare';					 --Same action as in TR1, except M16
'241: M16 gunflare';				 --Used when shooting M16 only
'242: ';
'243: Camera target';				--Same action as in TR1
'244: Waterfall mist';			   --As above
'245: Harpoon';
'246: ';
'247: Placeholder';				  --Purpose unknown
'248: Grenade (single)';			 --Projectile, generated when shooting grenadegun
'249: Harpoon (flying)';			 --Projectile, generated when shooting harpoon gun
'250: Lava particles';			   --Same action as in TR1
'251: Lava / air particle emitter';  --As above
'252: Flame';						--As above
'253: Flame emitter';				--As above
'254: Skybox';					   --Rendered when any room with outside flag set is visible
'255: Font graphics';
'256: Monk';
'257: Door bell';					--Plays sound ID #334 once
'258: Alarm bell';				   --Plays looped sound ID #335
'259: Helicopter';				   --Travels around 30 sectors, then deactivates itself
'260: Winston';
'261: ';
'262: Lara cutscene placement';	  --Used in last level to engage shower cutscene
'263: Shotgun animation';			--Used in end game cutscene (shower)
'264: Dragon explosion emitter';	 --Activates when Lara at a close range, produces animation sequence using entity models ID #209-211, then spawns entity ID #22 at the end
	}					  --needs add
	LIVENT = 0x0897A4
	NUMOFENT = 0		   --needs add
	OFFSET_FIRST_ENTITY = 0	   --needs add
	OFFSET_FIRST_ENTITY_ID = 0		--needs add
	OFFSET_FIRST_ENTITY_X = 0		 --needs add
	OFFSET_FIRST_ENTITY_Y = 0		 --needs add
	OFFSET_FIRST_ENTITY_Z = 0		 --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0	   --needs add
	SIZOFENT = 0		   --needs add

--QWOP:
	OFFSET_QWOP = 0x48

--Camera:
	CAMERAX = 0			--needs add
	CAMERAY = 0			--needs add
	CAMERAZ = 0			--needs add
	CAMERAROOM = 0		 --needs add
	CAM_ANGLE = 0		  --needs add
	
	CAMUPFUN = 0		   --needs add

--Other:
	OFFSET_ACT = 0x08B6CA
	OFFSET_TRIGGER = 0	   --needs add
	SPRINT = 0x0
end
--------------------------------------------------------------------------------------------------TR3------------------------------------------------------------------------------------------------------
-- Tomb Raider III (1998)
-- Tomb Raider III: Adventures of Lara Croft
-- First release date: November 1998

--Notes:
		--Time in Lara's invetory shows 2 frames less
		--Snakes don't count to living entities
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='5375F6CD' then	--TR3 (SLES_016.49) (UK)

--Basic informations:
	POINTER_LARA = 0x098F3C
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0			--needs add
	OFFSET_TIMER = 0x0A62BC
	OFFSET_TIMER1 = 0x0A5DFC   --(This version shows wrong final IGT time)
	TIMERLEV = 0x12
	FLIPMAP = 0			--needs add
	ORLEV = 0			  --needs add

--Entities:
	OFFSET_ENTITY = 0x098EC4
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Animations only, skin is now placed in ID #315
'1: Lara pistol animation';		  --Same action as in TR2
'2: Lara’s ponytail';				--As above
'3: Lara shotgun animation';		 --As above
'4: Lara Desert Eagle animation';	--One-handed gun animation
'5: Lara Uzi animation';			 --Same action as in TR2
'6: Lara MP5 animation';			 --As above
'7: Lara rocket launcher animation';
'8: Lara grenade gun animation';	 --Same action as in TR2
'9: Lara harpoon gun animation';	 --As above
'10: Lara flare animation';		  --As above
'11: Lara UPV animation';			--Animations for UPV vehicle
'12: UPV';						   --Vehicle
'13: Lara misc animations';
'14: Kayak';						 --As above (12)
'15: Inflatable boat';			   --As above (12)
'16: Quadbike';					  --As above (12)
'17: Mine cart';					 --As above (12)
'18: Big gun';					   --Mannable gun (Lara can operate it)
'19: Hydro propeller (?)';
'20: Tribesman with spiked axe';
'21: Tribesman with poison-dart gun';
'22: Dog';
'23: Rat';
'24: Kill All Triggers';			 --Deactivates all triggered entities, sometimes buggy
'25: Killer whale';
'26: Scuba diver';
'27: Crow';
'28: Tiger';
'29: Vulture';
'30: Assault-course target';
'31: Crawler mutant in closet';
'32: Crocodile (in water)';
'33: ';
'34: Compsognathus';
'35: Lizard man';
'36: Puna';
'37: Mercenary';
'38: Raptor hung by rope';		   --Used as fish bait in Crash Site (CRASH.TR2)
'39: RX-Tech guy in red jacket';
'40: RX-Tech guy with gun';		  --Dressed like flamethrower guy (ID #50)
'41: Dog (Antarctica)';
'42: Crawler mutant';
'43: ';
'44: Tinnos wasp';
'45: Tinnos monster';
'46: Brute mutant (with claw)';
'47: Tinnos wasp respawn point';	 --When activated, constantly generates specific number of wasps
'48: Raptor respawn point';		  --Hardcoded to respawn number of raptors previously unkilled in level
'49: Willard spider';
'50: RX-Tech flamethrower guy';
'51: London mercenary';
'52: ';
'53: Punk';						  --"Damned" stick-wielding goon
'54: ';
'55: ';
'56: London guard';
'57: Sophia Lee';
'58: Cleaner robot';				 --Also called "Thames Wharf machine"
'59: ';
'60: MP with stick';
'61: MP with gun';
'62: Prisoner';
'63: MP with sighted gun and night sight';
'64: Gun turret';
'65: Dam guard';
'66: Tripwire';
'67: Electrified wire';
'68: Killer tripwire';
'69: Cobra / Rattlesnake';
'70: Shiva';
'71: Monkey';
'72: ';
'73: Tony Firehands';
'74: AI Guard';
'75: AI Ambush';
'76: AI Patrol 1';
'77: AI Modify';
'78: AI Follow';
'79: AI Patrol 2';
'80: AI Path';					   --Used by Willard spider boss to circle around meteorite cavern
'81: AI Check';					  --Used by Willard spider boss to check if Lara is in artifact chamber and shoot if she’s in
'82: Unknown id 82';
'83: Falling block';
'84: ';
'85: ';
'86: Swinging thing';
'87: Teeth spikes / Barbed wire';
'88: Rolling ball / Barrel';
'89: Giant boulder';				 --Appears in Temple of Puna
'90: Disc';						  --Mesh is a leftover from TR2 discgun, engine uses wireframe darts instead
'91: Dart shooter';
'92: ';
'93: ';
'94: Skeleton trap / Slamming door';
'95: ';
'96: ';
'97: Pushable block 1';
'98: Pushable block 2';
'99: ';
'100: ';
'101: Destroyable boarded-up window';
'102: Destroyable boarded-up window / wall';
'103: ';
'104: ';
'105: ';
'106: Area 51 swinger';
'107: Falling ceiling';
'108: Rolling spindle';
'109: ';
'110: Subway train';
'111: Wall knife blade / Knife disk';
'112: ';
'113: Detachable stalactites';
'114: Spiky wall';				   --Same action as in TR2
'115: ';
'116: Spiky vertical wall / Tunnel borer';
'117: Valve wheel / Pulley';
'118: Small wall switch';
'119: Damaging animating 1';		 --Underwater propeller / Diver sitting on block / Underwater rotating knives / Meteorite, causes damage on collision
'120: Damaging animating 2';		 --Fan, causes damage on collision
'121: Damaging animating 3';		 --Heavy stamper / Grinding drum / Underwater rotating knives, causes damage on collision
'122: Shiva statue';				 --Original petrified state, never activates
'123: Monkey medipack meshswap';	 --Monkey head gets replaced by this when it picks up medipack
'124: Monkey key meshswap';		  --Monkey head gets replaced by this when it picks up key
'125: UI frame';					 --Used to create UI elements
'126: ';
'127: Zipline handle';			   --Same action as in TR2
'128: Push-button switch';
'129: Wall switch';
'130: Underwater switch';
'131: Door 1';
'132: Door 2';
'133: Door 3';
'134: Door 4';
'135: Door 5';
'136: Door 6';
'137: Door 7';
'138: Door 8';
'139: Trapdoor 1';
'140: Trapdoor 2';
'141: Trapdoor 3';
'142: Bridge flat';
'143: Bridge tilt 1';
'144: Bridge tilt 2';
'145: Passport (opening up)';
'146: Stopwatch';
'147: Lara’s Home photo';
'148: Cutscene actor 1';
'149: Cutscene actor 2';
'150: Cutscene actor 3';
'151: Cutscene actor 4';
'152: Cutscene actor 5';
'153: Cutscene actor 6';
'154: Cutscene actor 7';
'155: Cutscene actor 8';
'156: Cutscene actor 9';
'157: ';
'158: Passport (closed)';
'159: Map';
'160: Pistols';
'161: Shotgun';
'162: Desert Eagle';
'163: Uzis';
'164: Harpoon gun';
'165: MP5';
'166: Rocket launcher';
'167: Grenade launcher';
'168: Pistol ammo';
'169: Shotgun ammo';
'170: Desert Eagle ammo';
'171: Uzi ammo';
'172: Harpoons';
'173: MP5 ammo';
'174: Rockets';
'175: Grenades';
'176: Small medipack';
'177: Large medipack';
'178: Flares';
'179: Flare';						--Spawns when Lara throws flare away, re-pickupable
'180: Savegame crystal';
'181: Sunglasses';
'182: Portable CD Player';
'183: Direction keys';
'184: Globe';						--For indicating destinations in "Select level" dialog
'185: Pistols';
'186: Shotgun';
'187: Desert Eagle';
'188: Uzis';
'189: Harpoon gun';
'190: MP5';
'191: Rocket launcher';
'192: Grenade gun';
'193: Pistol ammo';
'194: Shotgun ammo';
'195: Desert Eagle ammo';
'196: Uzi ammo';
'197: Harpoons';
'198: MP5 ammo';
'199: Rockets';
'200: Grenades';
'201: Small medipack';
'202: Large medipack';
'203: Flares';
'204: Savegame crystal';
'205: Puzzle 1';
'206: Puzzle 2';
'207: Puzzle 3';
'208: Puzzle 4';
'209: Puzzle 1';
'210: Puzzle 2';
'211: Puzzle 3';
'212: Puzzle 4';
'213: Slot 1 empty';				 --Swaps to entity ID #217 when entity ID #209 is used
'214: Slot 2 empty';				 --Swaps to entity ID #218 when entity ID #210 is used
'215: Slot 3 empty';				 --Swaps to entity ID #219 when entity ID #211 is used
'216: Slot 4 empty';				 --Swaps to entity ID #220 when entity ID #212 is used
'217: Slot 1 full';
'218: Slot 2 full';
'219: Slot 3 full';
'220: Slot 4 full';
'221: ';
'222: ';
'223: ';
'224: Key 1';
'225: Key 2';
'226: Key 3';
'227: Key 4';
'228: Key 1';
'229: Key 2';
'230: Key 3';
'231: Key 4';
'232: Keyhole 1';					--Used with entity ID #224
'233: Keyhole 2';					--Used with entity ID #225
'234: Keyhole 3';					--Used with entity ID #226
'235: Keyhole 4';					--Used with entity ID #227
'236: Quest item 1';
'237: Quest item 2';				 --No effect on actual inventory
'238: Quest item 1';
'239: Quest item 2';				 --No effect on actual inventory
'240: Infada stone';
'241: Element 115';
'242: Eye of Isis';
'243: Ora dagger';
'244: Infada stone';
'245: Element 115';
'246: Eye of Isis';
'247: Ora dagger';
'248: ';
'249: ';
'250: ';
'251: ';
'252: ';
'253: ';
'254: ';
'255: ';
'256: ';
'257: ';
'258: ';
'259: ';
'260: ';
'261: ';
'262: ';
'263: ';
'264: ';
'265: ';
'266: ';
'267: ';
'268: ';
'269: ';
'270: ';
'271: ';
'272: Keys (sprite)';
'273: Keys (sprite)';
'274: ';
'275: ';
'276: Infada stone';
'277: Element 115';
'278: Eye of Isis';
'279: Ora dagger';
'280: ';
'281: ';
'282: Fire-breathing dragon statue';
'283: ';
'284: ';
'285: Unknown visible 285';
'286: ';
'287: Tyrannosaur';
'288: Raptor';
'289: ';
'290: ';
'291: Laser sweeper';
'292: Electrified Field';
'293: ';
'294: Shadow sprit';				 --Applied as a blob shadow with subtractive blending
'295: Detonator switch box';
'296: Misc sprites';				 --Sprites for different purposes merged into single object
'297: Bubble';
'298: ';
'299: Glow';
'300: Gunflare';
'301: Gunflare (MP5)';
'302: ';
'303: ';
'304: Look At item';
'305: Waterfall mist';
'306: Harpoon (single)';
'307: ';
'308: ';
'309: Rocket (single)';
'310: Harpoon (single)';
'311: Grenade (single)';
'312: Big missile';
'313: Smoke';
'314: Movable Boom';
'315: Lara skin';
'316: Glow 2';
'317: Unknown visible 317';
'318: Alarm light';
'319: Light';
'320: ';
'321: Light 2';
'322: Pulsating Light';
'323: ';
'324: Red Light';
'325: Green Light';
'326: Blue Light';
'327: Light 3';
'328: Light 4';
'329: ';
'330: Fire';
'331: Alternate Fire';
'332: Alternate Fire 2';
'333: Fire 2';
'334: Smoke 2';
'335: Smoke 3';
'336: Smoke 4';
'337: Greenish Smoke';
'338: Pirahnas';
'339: Fish';
'340: ';
'341: ';
'342: ';
'343: ';
'344: ';
'345: ';
'346: ';
'347: Bat swarm';
'348: ';
'349: Animating 1';
'350: Animating 2';
'351: Animating 3';
'352: Animating 4';
'353: Animating 5';
'354: Animating 6';
'355: Skybox';
'356: Font graphics';
'357: Doorbell';					 --Produces no actual sound
'358: Unknown id 358';
'359: ';
'360: Winston';
'361: Winston in camo suit';		 --Deactivates entity ID #360 when triggered
'362: Timer font graphics';		  --Used in Assault Course and racetrack
'363: ';
'364: ';
'365: Earthquake';
'366: Yellow shell casing';		  --Spawned when firing pistols, uzis, MP5 or Desert Eagle
'367: Red shell casing';			 --Spawned when firing shotgun
'368: ';
'369: ';
'370: Tinnos light shaft';
'371: ';
'372: ';
'373: Electrical switch box';
	}					  --needs add
	LIVENT = 0x097A54
	NUMOFENT = 0		   --needs add
	OFFSET_FIRST_ENTITY = 0	   --needs add
	OFFSET_FIRST_ENTITY_ID = 0		--needs add
	OFFSET_FIRST_ENTITY_X = 0		 --needs add
	OFFSET_FIRST_ENTITY_Y = 0		 --needs add
	OFFSET_FIRST_ENTITY_Z = 0		 --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0	   --needs add
	SIZOFENT = 0		   --needs add

--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0			--needs add
	CAMERAY = 0			--needs add
	CAMERAZ = 0			--needs add
	CAMERAROOM = 0		 --needs add
	CAM_ANGLE = 0		  --needs add
	
	CAMUPFUN = 0		   --needs add

--Other:
	OFFSET_ACT = 0x09ACA0
	OFFSET_TRIGGER = 0x098F9C
	SPRINT = 0x3
end
	
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='FC8FB87A' then	--TR3 (SLUS_006.91) (USA)

--Basic informations:
	POINTER_LARA = 0x9961C
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0			--needs add
	OFFSET_TIMER = 0xA699C
	OFFSET_TIMER1 = 0x0A64DC
	TIMERLEV = 0x12
	FLIPMAP = 0			--needs add
	ORLEV = 0x096632

--Entities:
	OFFSET_ENTITY = 0x995A4
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Animations only, skin is now placed in ID #315
'1: Lara pistol animation';		  --Same action as in TR2
'2: Lara’s ponytail';				--As above
'3: Lara shotgun animation';		 --As above
'4: Lara Desert Eagle animation';	--One-handed gun animation
'5: Lara Uzi animation';			 --Same action as in TR2
'6: Lara MP5 animation';			 --As above
'7: Lara rocket launcher animation';
'8: Lara grenade gun animation';	 --Same action as in TR2
'9: Lara harpoon gun animation';	 --As above
'10: Lara flare animation';		  --As above
'11: Lara UPV animation';			--Animations for UPV vehicle
'12: UPV';						   --Vehicle
'13: Lara misc animations';
'14: Kayak';						 --As above (12)
'15: Inflatable boat';			   --As above (12)
'16: Quadbike';					  --As above (12)
'17: Mine cart';					 --As above (12)
'18: Big gun';					   --Mannable gun (Lara can operate it)
'19: Hydro propeller (?)';
'20: Tribesman with spiked axe';
'21: Tribesman with poison-dart gun';
'22: Dog';
'23: Rat';
'24: Kill All Triggers';			 --Deactivates all triggered entities, sometimes buggy
'25: Killer whale';
'26: Scuba diver';
'27: Crow';
'28: Tiger';
'29: Vulture';
'30: Assault-course target';
'31: Crawler mutant in closet';
'32: Crocodile (in water)';
'33: ';
'34: Compsognathus';
'35: Lizard man';
'36: Puna';
'37: Mercenary';
'38: Raptor hung by rope';		   --Used as fish bait in Crash Site (CRASH.TR2)
'39: RX-Tech guy in red jacket';
'40: RX-Tech guy with gun';		  --Dressed like flamethrower guy (ID #50)
'41: Dog (Antarctica)';
'42: Crawler mutant';
'43: ';
'44: Tinnos wasp';
'45: Tinnos monster';
'46: Brute mutant (with claw)';
'47: Tinnos wasp respawn point';	 --When activated, constantly generates specific number of wasps
'48: Raptor respawn point';		  --Hardcoded to respawn number of raptors previously unkilled in level
'49: Willard spider';
'50: RX-Tech flamethrower guy';
'51: London mercenary';
'52: ';
'53: Punk';						  --"Damned" stick-wielding goon
'54: ';
'55: ';
'56: London guard';
'57: Sophia Lee';
'58: Cleaner robot';				 --Also called "Thames Wharf machine"
'59: ';
'60: MP with stick';
'61: MP with gun';
'62: Prisoner';
'63: MP with sighted gun and night sight';
'64: Gun turret';
'65: Dam guard';
'66: Tripwire';
'67: Electrified wire';
'68: Killer tripwire';
'69: Cobra / Rattlesnake';
'70: Shiva';
'71: Monkey';
'72: ';
'73: Tony Firehands';
'74: AI Guard';
'75: AI Ambush';
'76: AI Patrol 1';
'77: AI Modify';
'78: AI Follow';
'79: AI Patrol 2';
'80: AI Path';					   --Used by Willard spider boss to circle around meteorite cavern
'81: AI Check';					  --Used by Willard spider boss to check if Lara is in artifact chamber and shoot if she’s in
'82: Unknown id 82';
'83: Falling block';
'84: ';
'85: ';
'86: Swinging thing';
'87: Teeth spikes / Barbed wire';
'88: Rolling ball / Barrel';
'89: Giant boulder';				 --Appears in Temple of Puna
'90: Disc';						  --Mesh is a leftover from TR2 discgun, engine uses wireframe darts instead
'91: Dart shooter';
'92: ';
'93: ';
'94: Skeleton trap / Slamming door';
'95: ';
'96: ';
'97: Pushable block 1';
'98: Pushable block 2';
'99: ';
'100: ';
'101: Destroyable boarded-up window';
'102: Destroyable boarded-up window / wall';
'103: ';
'104: ';
'105: ';
'106: Area 51 swinger';
'107: Falling ceiling';
'108: Rolling spindle';
'109: ';
'110: Subway train';
'111: Wall knife blade / Knife disk';
'112: ';
'113: Detachable stalactites';
'114: Spiky wall';				   --Same action as in TR2
'115: ';
'116: Spiky vertical wall / Tunnel borer';
'117: Valve wheel / Pulley';
'118: Small wall switch';
'119: Damaging animating 1';		 --Underwater propeller / Diver sitting on block / Underwater rotating knives / Meteorite, causes damage on collision
'120: Damaging animating 2';		 --Fan, causes damage on collision
'121: Damaging animating 3';		 --Heavy stamper / Grinding drum / Underwater rotating knives, causes damage on collision
'122: Shiva statue';				 --Original petrified state, never activates
'123: Monkey medipack meshswap';	 --Monkey head gets replaced by this when it picks up medipack
'124: Monkey key meshswap';		  --Monkey head gets replaced by this when it picks up key
'125: UI frame';					 --Used to create UI elements
'126: ';
'127: Zipline handle';			   --Same action as in TR2
'128: Push-button switch';
'129: Wall switch';
'130: Underwater switch';
'131: Door 1';
'132: Door 2';
'133: Door 3';
'134: Door 4';
'135: Door 5';
'136: Door 6';
'137: Door 7';
'138: Door 8';
'139: Trapdoor 1';
'140: Trapdoor 2';
'141: Trapdoor 3';
'142: Bridge flat';
'143: Bridge tilt 1';
'144: Bridge tilt 2';
'145: Passport (opening up)';
'146: Stopwatch';
'147: Lara’s Home photo';
'148: Cutscene actor 1';
'149: Cutscene actor 2';
'150: Cutscene actor 3';
'151: Cutscene actor 4';
'152: Cutscene actor 5';
'153: Cutscene actor 6';
'154: Cutscene actor 7';
'155: Cutscene actor 8';
'156: Cutscene actor 9';
'157: ';
'158: Passport (closed)';
'159: Map';
'160: Pistols';
'161: Shotgun';
'162: Desert Eagle';
'163: Uzis';
'164: Harpoon gun';
'165: MP5';
'166: Rocket launcher';
'167: Grenade launcher';
'168: Pistol ammo';
'169: Shotgun ammo';
'170: Desert Eagle ammo';
'171: Uzi ammo';
'172: Harpoons';
'173: MP5 ammo';
'174: Rockets';
'175: Grenades';
'176: Small medipack';
'177: Large medipack';
'178: Flares';
'179: Flare';						--Spawns when Lara throws flare away, re-pickupable
'180: Savegame crystal';
'181: Sunglasses';
'182: Portable CD Player';
'183: Direction keys';
'184: Globe';						--For indicating destinations in "Select level" dialog
'185: Pistols';
'186: Shotgun';
'187: Desert Eagle';
'188: Uzis';
'189: Harpoon gun';
'190: MP5';
'191: Rocket launcher';
'192: Grenade gun';
'193: Pistol ammo';
'194: Shotgun ammo';
'195: Desert Eagle ammo';
'196: Uzi ammo';
'197: Harpoons';
'198: MP5 ammo';
'199: Rockets';
'200: Grenades';
'201: Small medipack';
'202: Large medipack';
'203: Flares';
'204: Savegame crystal';
'205: Puzzle 1';
'206: Puzzle 2';
'207: Puzzle 3';
'208: Puzzle 4';
'209: Puzzle 1';
'210: Puzzle 2';
'211: Puzzle 3';
'212: Puzzle 4';
'213: Slot 1 empty';				 --Swaps to entity ID #217 when entity ID #209 is used
'214: Slot 2 empty';				 --Swaps to entity ID #218 when entity ID #210 is used
'215: Slot 3 empty';				 --Swaps to entity ID #219 when entity ID #211 is used
'216: Slot 4 empty';				 --Swaps to entity ID #220 when entity ID #212 is used
'217: Slot 1 full';
'218: Slot 2 full';
'219: Slot 3 full';
'220: Slot 4 full';
'221: ';
'222: ';
'223: ';
'224: Key 1';
'225: Key 2';
'226: Key 3';
'227: Key 4';
'228: Key 1';
'229: Key 2';
'230: Key 3';
'231: Key 4';
'232: Keyhole 1';					--Used with entity ID #224
'233: Keyhole 2';					--Used with entity ID #225
'234: Keyhole 3';					--Used with entity ID #226
'235: Keyhole 4';					--Used with entity ID #227
'236: Quest item 1';
'237: Quest item 2';				 --No effect on actual inventory
'238: Quest item 1';
'239: Quest item 2';				 --No effect on actual inventory
'240: Infada stone';
'241: Element 115';
'242: Eye of Isis';
'243: Ora dagger';
'244: Infada stone';
'245: Element 115';
'246: Eye of Isis';
'247: Ora dagger';
'248: ';
'249: ';
'250: ';
'251: ';
'252: ';
'253: ';
'254: ';
'255: ';
'256: ';
'257: ';
'258: ';
'259: ';
'260: ';
'261: ';
'262: ';
'263: ';
'264: ';
'265: ';
'266: ';
'267: ';
'268: ';
'269: ';
'270: ';
'271: ';
'272: Keys (sprite)';
'273: Keys (sprite)';
'274: ';
'275: ';
'276: Infada stone';
'277: Element 115';
'278: Eye of Isis';
'279: Ora dagger';
'280: ';
'281: ';
'282: Fire-breathing dragon statue';
'283: ';
'284: ';
'285: Unknown visible 285';
'286: ';
'287: Tyrannosaur';
'288: Raptor';
'289: ';
'290: ';
'291: Laser sweeper';
'292: Electrified Field';
'293: ';
'294: Shadow sprit';				 --Applied as a blob shadow with subtractive blending
'295: Detonator switch box';
'296: Misc sprites';				 --Sprites for different purposes merged into single object
'297: Bubble';
'298: ';
'299: Glow';
'300: Gunflare';
'301: Gunflare (MP5)';
'302: ';
'303: ';
'304: Look At item';
'305: Waterfall mist';
'306: Harpoon (single)';
'307: ';
'308: ';
'309: Rocket (single)';
'310: Harpoon (single)';
'311: Grenade (single)';
'312: Big missile';
'313: Smoke';
'314: Movable Boom';
'315: Lara skin';
'316: Glow 2';
'317: Unknown visible 317';
'318: Alarm light';
'319: Light';
'320: ';
'321: Light 2';
'322: Pulsating Light';
'323: ';
'324: Red Light';
'325: Green Light';
'326: Blue Light';
'327: Light 3';
'328: Light 4';
'329: ';
'330: Fire';
'331: Alternate Fire';
'332: Alternate Fire 2';
'333: Fire 2';
'334: Smoke 2';
'335: Smoke 3';
'336: Smoke 4';
'337: Greenish Smoke';
'338: Pirahnas';
'339: Fish';
'340: ';
'341: ';
'342: ';
'343: ';
'344: ';
'345: ';
'346: ';
'347: Bat swarm';
'348: ';
'349: Animating 1';
'350: Animating 2';
'351: Animating 3';
'352: Animating 4';
'353: Animating 5';
'354: Animating 6';
'355: Skybox';
'356: Font graphics';
'357: Doorbell';					 --Produces no actual sound
'358: Unknown id 358';
'359: ';
'360: Winston';
'361: Winston in camo suit';		 --Deactivates entity ID #360 when triggered
'362: Timer font graphics';		  --Used in Assault Course and racetrack
'363: ';
'364: ';
'365: Earthquake';
'366: Yellow shell casing';		  --Spawned when firing pistols, uzis, MP5 or Desert Eagle
'367: Red shell casing';			 --Spawned when firing shotgun
'368: ';
'369: ';
'370: Tinnos light shaft';
'371: ';
'372: ';
'373: Electrical switch box';
	}					   --needs add
	LIVENT = 0			  --needs add
	NUMOFENT = 0			--needs add
	OFFSET_FIRST_ENTITY = 0		--needs add
	OFFSET_FIRST_ENTITY_ID = 0		 --needs add
	OFFSET_FIRST_ENTITY_X = 0		  --needs add
	OFFSET_FIRST_ENTITY_Y = 0		  --needs add
	OFFSET_FIRST_ENTITY_Z = 0		  --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0		--needs add
	SIZOFENT = 0			--needs add

--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0			 --needs add
	CAMERAY = 0			 --needs add
	CAMERAZ = 0			 --needs add
	CAMERAROOM = 0		  --needs add
	CAM_ANGLE = 0		   --needs add
	
	CAMUPFUN = 0			--needs add

--Other:
	OFFSET_ACT = 0x09B380
	OFFSET_TRIGGER = 0x09967C
	SPRINT = 0x3
end

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='807A3108' then	--TR3 (SLUS_006.91) (USA)

--Basic informations:
	POINTER_LARA = 0x9923C
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0			 --needs add
	OFFSET_TIMER = 0x0A65BC
	OFFSET_TIMER1 = 0x0A60FC
	TIMERLEV = 0x12
	FLIPMAP = 0			 --needs add
	ORLEV = 0			   --needs add

--Entities:
	OFFSET_ENTITY = 0x0991C4
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Animations only, skin is now placed in ID #315
'1: Lara pistol animation';		  --Same action as in TR2
'2: Lara’s ponytail';				--As above
'3: Lara shotgun animation';		 --As above
'4: Lara Desert Eagle animation';	--One-handed gun animation
'5: Lara Uzi animation';			 --Same action as in TR2
'6: Lara MP5 animation';			 --As above
'7: Lara rocket launcher animation';
'8: Lara grenade gun animation';	 --Same action as in TR2
'9: Lara harpoon gun animation';	 --As above
'10: Lara flare animation';		  --As above
'11: Lara UPV animation';			--Animations for UPV vehicle
'12: UPV';						   --Vehicle
'13: Lara misc animations';
'14: Kayak';						 --As above (12)
'15: Inflatable boat';			   --As above (12)
'16: Quadbike';					  --As above (12)
'17: Mine cart';					 --As above (12)
'18: Big gun';					   --Mannable gun (Lara can operate it)
'19: Hydro propeller (?)';
'20: Tribesman with spiked axe';
'21: Tribesman with poison-dart gun';
'22: Dog';
'23: Rat';
'24: Kill All Triggers';			 --Deactivates all triggered entities, sometimes buggy
'25: Killer whale';
'26: Scuba diver';
'27: Crow';
'28: Tiger';
'29: Vulture';
'30: Assault-course target';
'31: Crawler mutant in closet';
'32: Crocodile (in water)';
'33: ';
'34: Compsognathus';
'35: Lizard man';
'36: Puna';
'37: Mercenary';
'38: Raptor hung by rope';		   --Used as fish bait in Crash Site (CRASH.TR2)
'39: RX-Tech guy in red jacket';
'40: RX-Tech guy with gun';		  --Dressed like flamethrower guy (ID #50)
'41: Dog (Antarctica)';
'42: Crawler mutant';
'43: ';
'44: Tinnos wasp';
'45: Tinnos monster';
'46: Brute mutant (with claw)';
'47: Tinnos wasp respawn point';	 --When activated, constantly generates specific number of wasps
'48: Raptor respawn point';		  --Hardcoded to respawn number of raptors previously unkilled in level
'49: Willard spider';
'50: RX-Tech flamethrower guy';
'51: London mercenary';
'52: ';
'53: Punk';						  --"Damned" stick-wielding goon
'54: ';
'55: ';
'56: London guard';
'57: Sophia Lee';
'58: Cleaner robot';				 --Also called "Thames Wharf machine"
'59: ';
'60: MP with stick';
'61: MP with gun';
'62: Prisoner';
'63: MP with sighted gun and night sight';
'64: Gun turret';
'65: Dam guard';
'66: Tripwire';
'67: Electrified wire';
'68: Killer tripwire';
'69: Cobra / Rattlesnake';
'70: Shiva';
'71: Monkey';
'72: ';
'73: Tony Firehands';
'74: AI Guard';
'75: AI Ambush';
'76: AI Patrol 1';
'77: AI Modify';
'78: AI Follow';
'79: AI Patrol 2';
'80: AI Path';					   --Used by Willard spider boss to circle around meteorite cavern
'81: AI Check';					  --Used by Willard spider boss to check if Lara is in artifact chamber and shoot if she’s in
'82: Unknown id 82';
'83: Falling block';
'84: ';
'85: ';
'86: Swinging thing';
'87: Teeth spikes / Barbed wire';
'88: Rolling ball / Barrel';
'89: Giant boulder';				 --Appears in Temple of Puna
'90: Disc';						  --Mesh is a leftover from TR2 discgun, engine uses wireframe darts instead
'91: Dart shooter';
'92: ';
'93: ';
'94: Skeleton trap / Slamming door';
'95: ';
'96: ';
'97: Pushable block 1';
'98: Pushable block 2';
'99: ';
'100: ';
'101: Destroyable boarded-up window';
'102: Destroyable boarded-up window / wall';
'103: ';
'104: ';
'105: ';
'106: Area 51 swinger';
'107: Falling ceiling';
'108: Rolling spindle';
'109: ';
'110: Subway train';
'111: Wall knife blade / Knife disk';
'112: ';
'113: Detachable stalactites';
'114: Spiky wall';				   --Same action as in TR2
'115: ';
'116: Spiky vertical wall / Tunnel borer';
'117: Valve wheel / Pulley';
'118: Small wall switch';
'119: Damaging animating 1';		 --Underwater propeller / Diver sitting on block / Underwater rotating knives / Meteorite, causes damage on collision
'120: Damaging animating 2';		 --Fan, causes damage on collision
'121: Damaging animating 3';		 --Heavy stamper / Grinding drum / Underwater rotating knives, causes damage on collision
'122: Shiva statue';				 --Original petrified state, never activates
'123: Monkey medipack meshswap';	 --Monkey head gets replaced by this when it picks up medipack
'124: Monkey key meshswap';		  --Monkey head gets replaced by this when it picks up key
'125: UI frame';					 --Used to create UI elements
'126: ';
'127: Zipline handle';			   --Same action as in TR2
'128: Push-button switch';
'129: Wall switch';
'130: Underwater switch';
'131: Door 1';
'132: Door 2';
'133: Door 3';
'134: Door 4';
'135: Door 5';
'136: Door 6';
'137: Door 7';
'138: Door 8';
'139: Trapdoor 1';
'140: Trapdoor 2';
'141: Trapdoor 3';
'142: Bridge flat';
'143: Bridge tilt 1';
'144: Bridge tilt 2';
'145: Passport (opening up)';
'146: Stopwatch';
'147: Lara’s Home photo';
'148: Cutscene actor 1';
'149: Cutscene actor 2';
'150: Cutscene actor 3';
'151: Cutscene actor 4';
'152: Cutscene actor 5';
'153: Cutscene actor 6';
'154: Cutscene actor 7';
'155: Cutscene actor 8';
'156: Cutscene actor 9';
'157: ';
'158: Passport (closed)';
'159: Map';
'160: Pistols';
'161: Shotgun';
'162: Desert Eagle';
'163: Uzis';
'164: Harpoon gun';
'165: MP5';
'166: Rocket launcher';
'167: Grenade launcher';
'168: Pistol ammo';
'169: Shotgun ammo';
'170: Desert Eagle ammo';
'171: Uzi ammo';
'172: Harpoons';
'173: MP5 ammo';
'174: Rockets';
'175: Grenades';
'176: Small medipack';
'177: Large medipack';
'178: Flares';
'179: Flare';						--Spawns when Lara throws flare away, re-pickupable
'180: Savegame crystal';
'181: Sunglasses';
'182: Portable CD Player';
'183: Direction keys';
'184: Globe';						--For indicating destinations in "Select level" dialog
'185: Pistols';
'186: Shotgun';
'187: Desert Eagle';
'188: Uzis';
'189: Harpoon gun';
'190: MP5';
'191: Rocket launcher';
'192: Grenade gun';
'193: Pistol ammo';
'194: Shotgun ammo';
'195: Desert Eagle ammo';
'196: Uzi ammo';
'197: Harpoons';
'198: MP5 ammo';
'199: Rockets';
'200: Grenades';
'201: Small medipack';
'202: Large medipack';
'203: Flares';
'204: Savegame crystal';
'205: Puzzle 1';
'206: Puzzle 2';
'207: Puzzle 3';
'208: Puzzle 4';
'209: Puzzle 1';
'210: Puzzle 2';
'211: Puzzle 3';
'212: Puzzle 4';
'213: Slot 1 empty';				 --Swaps to entity ID #217 when entity ID #209 is used
'214: Slot 2 empty';				 --Swaps to entity ID #218 when entity ID #210 is used
'215: Slot 3 empty';				 --Swaps to entity ID #219 when entity ID #211 is used
'216: Slot 4 empty';				 --Swaps to entity ID #220 when entity ID #212 is used
'217: Slot 1 full';
'218: Slot 2 full';
'219: Slot 3 full';
'220: Slot 4 full';
'221: ';
'222: ';
'223: ';
'224: Key 1';
'225: Key 2';
'226: Key 3';
'227: Key 4';
'228: Key 1';
'229: Key 2';
'230: Key 3';
'231: Key 4';
'232: Keyhole 1';					--Used with entity ID #224
'233: Keyhole 2';					--Used with entity ID #225
'234: Keyhole 3';					--Used with entity ID #226
'235: Keyhole 4';					--Used with entity ID #227
'236: Quest item 1';
'237: Quest item 2';				 --No effect on actual inventory
'238: Quest item 1';
'239: Quest item 2';				 --No effect on actual inventory
'240: Infada stone';
'241: Element 115';
'242: Eye of Isis';
'243: Ora dagger';
'244: Infada stone';
'245: Element 115';
'246: Eye of Isis';
'247: Ora dagger';
'248: ';
'249: ';
'250: ';
'251: ';
'252: ';
'253: ';
'254: ';
'255: ';
'256: ';
'257: ';
'258: ';
'259: ';
'260: ';
'261: ';
'262: ';
'263: ';
'264: ';
'265: ';
'266: ';
'267: ';
'268: ';
'269: ';
'270: ';
'271: ';
'272: Keys (sprite)';
'273: Keys (sprite)';
'274: ';
'275: ';
'276: Infada stone';
'277: Element 115';
'278: Eye of Isis';
'279: Ora dagger';
'280: ';
'281: ';
'282: Fire-breathing dragon statue';
'283: ';
'284: ';
'285: Unknown visible 285';
'286: ';
'287: Tyrannosaur';
'288: Raptor';
'289: ';
'290: ';
'291: Laser sweeper';
'292: Electrified Field';
'293: ';
'294: Shadow sprit';				 --Applied as a blob shadow with subtractive blending
'295: Detonator switch box';
'296: Misc sprites';				 --Sprites for different purposes merged into single object
'297: Bubble';
'298: ';
'299: Glow';
'300: Gunflare';
'301: Gunflare (MP5)';
'302: ';
'303: ';
'304: Look At item';
'305: Waterfall mist';
'306: Harpoon (single)';
'307: ';
'308: ';
'309: Rocket (single)';
'310: Harpoon (single)';
'311: Grenade (single)';
'312: Big missile';
'313: Smoke';
'314: Movable Boom';
'315: Lara skin';
'316: Glow 2';
'317: Unknown visible 317';
'318: Alarm light';
'319: Light';
'320: ';
'321: Light 2';
'322: Pulsating Light';
'323: ';
'324: Red Light';
'325: Green Light';
'326: Blue Light';
'327: Light 3';
'328: Light 4';
'329: ';
'330: Fire';
'331: Alternate Fire';
'332: Alternate Fire 2';
'333: Fire 2';
'334: Smoke 2';
'335: Smoke 3';
'336: Smoke 4';
'337: Greenish Smoke';
'338: Pirahnas';
'339: Fish';
'340: ';
'341: ';
'342: ';
'343: ';
'344: ';
'345: ';
'346: ';
'347: Bat swarm';
'348: ';
'349: Animating 1';
'350: Animating 2';
'351: Animating 3';
'352: Animating 4';
'353: Animating 5';
'354: Animating 6';
'355: Skybox';
'356: Font graphics';
'357: Doorbell';					 --Produces no actual sound
'358: Unknown id 358';
'359: ';
'360: Winston';
'361: Winston in camo suit';		 --Deactivates entity ID #360 when triggered
'362: Timer font graphics';		  --Used in Assault Course and racetrack
'363: ';
'364: ';
'365: Earthquake';
'366: Yellow shell casing';		  --Spawned when firing pistols, uzis, MP5 or Desert Eagle
'367: Red shell casing';			 --Spawned when firing shotgun
'368: ';
'369: ';
'370: Tinnos light shaft';
'371: ';
'372: ';
'373: Electrical switch box';
	}					 --needs add
	LIVENT = 0x097D54
	NUMOFENT = 0		  --needs add
	OFFSET_FIRST_ENTITY = 0	  --needs add
	OFFSET_FIRST_ENTITY_ID = 0	   --needs add
	OFFSET_FIRST_ENTITY_X = 0		--needs add
	OFFSET_FIRST_ENTITY_Y = 0		--needs add
	OFFSET_FIRST_ENTITY_Z = 0		--needs add
	OFFSET_FIRST_ENTITY_ROOM = 0	  --needs add
	SIZOFENT = 0		  --needs add

--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0		   --needs add
	CAMERAY = 0		   --needs add
	CAMERAZ = 0		   --needs add
	CAMERAROOM = 0		--needs add
	CAM_ANGLE = 0		 --needs add
	
	CAMUPFUN = 0		  --needs add

--Other:
	OFFSET_ACT = 0x09AFA0
	OFFSET_TRIGGER = 0	  --needs add
	SPRINT = 0x3
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='4F422104' then	--TR3 (SLPM_861.96) (JAP)

--Basic informations:
	POINTER_LARA = 0x99AB0
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0x099F88
	OFFSET_TIMER = 0xA6EA4
	OFFSET_TIMER1 = 0x0A69E4
	TIMERLEV = 0x12
	FLIPMAP = 0x099AE0
	ORLEV = 0			 --needs add

--Entities:
	OFFSET_ENTITY = 0x99A38
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';						   --Animations only, skin is now placed in ID #315
'1: Lara pistol animation';		  --Same action as in TR2
'2: Lara’s ponytail';				--As above
'3: Lara shotgun animation';		 --As above
'4: Lara Desert Eagle animation';	--One-handed gun animation
'5: Lara Uzi animation';			 --Same action as in TR2
'6: Lara MP5 animation';			 --As above
'7: Lara rocket launcher animation';
'8: Lara grenade gun animation';	 --Same action as in TR2
'9: Lara harpoon gun animation';	 --As above
'10: Lara flare animation';		  --As above
'11: Lara UPV animation';			--Animations for UPV vehicle
'12: UPV';						   --Vehicle
'13: Lara misc animations';
'14: Kayak';						 --As above (12)
'15: Inflatable boat';			   --As above (12)
'16: Quadbike';					  --As above (12)
'17: Mine cart';					 --As above (12)
'18: Big gun';					   --Mannable gun (Lara can operate it)
'19: Hydro propeller (?)';
'20: Tribesman with spiked axe';
'21: Tribesman with poison-dart gun';
'22: Dog';
'23: Rat';
'24: Kill All Triggers';			 --Deactivates all triggered entities, sometimes buggy
'25: Killer whale';
'26: Scuba diver';
'27: Crow';
'28: Tiger';
'29: Vulture';
'30: Assault-course target';
'31: Crawler mutant in closet';
'32: Crocodile (in water)';
'33: ';
'34: Compsognathus';
'35: Lizard man';
'36: Puna';
'37: Mercenary';
'38: Raptor hung by rope';		   --Used as fish bait in Crash Site (CRASH.TR2)
'39: RX-Tech guy in red jacket';
'40: RX-Tech guy with gun';		  --Dressed like flamethrower guy (ID #50)
'41: Dog (Antarctica)';
'42: Crawler mutant';
'43: ';
'44: Tinnos wasp';
'45: Tinnos monster';
'46: Brute mutant (with claw)';
'47: Tinnos wasp respawn point';	 --When activated, constantly generates specific number of wasps
'48: Raptor respawn point';		  --Hardcoded to respawn number of raptors previously unkilled in level
'49: Willard spider';
'50: RX-Tech flamethrower guy';
'51: London mercenary';
'52: ';
'53: Punk';						  --"Damned" stick-wielding goon
'54: ';
'55: ';
'56: London guard';
'57: Sophia Lee';
'58: Cleaner robot';				 --Also called "Thames Wharf machine"
'59: ';
'60: MP with stick';
'61: MP with gun';
'62: Prisoner';
'63: MP with sighted gun and night sight';
'64: Gun turret';
'65: Dam guard';
'66: Tripwire';
'67: Electrified wire';
'68: Killer tripwire';
'69: Cobra / Rattlesnake';
'70: Shiva';
'71: Monkey';
'72: ';
'73: Tony Firehands';
'74: AI Guard';
'75: AI Ambush';
'76: AI Patrol 1';
'77: AI Modify';
'78: AI Follow';
'79: AI Patrol 2';
'80: AI Path';					   --Used by Willard spider boss to circle around meteorite cavern
'81: AI Check';					  --Used by Willard spider boss to check if Lara is in artifact chamber and shoot if she’s in
'82: Unknown id 82';
'83: Falling block';
'84: ';
'85: ';
'86: Swinging thing';
'87: Teeth spikes / Barbed wire';
'88: Rolling ball / Barrel';
'89: Giant boulder';				 --Appears in Temple of Puna
'90: Disc';						  --Mesh is a leftover from TR2 discgun, engine uses wireframe darts instead
'91: Dart shooter';
'92: ';
'93: ';
'94: Skeleton trap / Slamming door';
'95: ';
'96: ';
'97: Pushable block 1';
'98: Pushable block 2';
'99: ';
'100: ';
'101: Destroyable boarded-up window';
'102: Destroyable boarded-up window / wall';
'103: ';
'104: ';
'105: ';
'106: Area 51 swinger';
'107: Falling ceiling';
'108: Rolling spindle';
'109: ';
'110: Subway train';
'111: Wall knife blade / Knife disk';
'112: ';
'113: Detachable stalactites';
'114: Spiky wall';				   --Same action as in TR2
'115: ';
'116: Spiky vertical wall / Tunnel borer';
'117: Valve wheel / Pulley';
'118: Small wall switch';
'119: Damaging animating 1';		 --Underwater propeller / Diver sitting on block / Underwater rotating knives / Meteorite, causes damage on collision
'120: Damaging animating 2';		 --Fan, causes damage on collision
'121: Damaging animating 3';		 --Heavy stamper / Grinding drum / Underwater rotating knives, causes damage on collision
'122: Shiva statue';				 --Original petrified state, never activates
'123: Monkey medipack meshswap';	 --Monkey head gets replaced by this when it picks up medipack
'124: Monkey key meshswap';		  --Monkey head gets replaced by this when it picks up key
'125: UI frame';					 --Used to create UI elements
'126: ';
'127: Zipline handle';			   --Same action as in TR2
'128: Push-button switch';
'129: Wall switch';
'130: Underwater switch';
'131: Door 1';
'132: Door 2';
'133: Door 3';
'134: Door 4';
'135: Door 5';
'136: Door 6';
'137: Door 7';
'138: Door 8';
'139: Trapdoor 1';
'140: Trapdoor 2';
'141: Trapdoor 3';
'142: Bridge flat';
'143: Bridge tilt 1';
'144: Bridge tilt 2';
'145: Passport (opening up)';
'146: Stopwatch';
'147: Lara’s Home photo';
'148: Cutscene actor 1';
'149: Cutscene actor 2';
'150: Cutscene actor 3';
'151: Cutscene actor 4';
'152: Cutscene actor 5';
'153: Cutscene actor 6';
'154: Cutscene actor 7';
'155: Cutscene actor 8';
'156: Cutscene actor 9';
'157: ';
'158: Passport (closed)';
'159: Map';
'160: Pistols';
'161: Shotgun';
'162: Desert Eagle';
'163: Uzis';
'164: Harpoon gun';
'165: MP5';
'166: Rocket launcher';
'167: Grenade launcher';
'168: Pistol ammo';
'169: Shotgun ammo';
'170: Desert Eagle ammo';
'171: Uzi ammo';
'172: Harpoons';
'173: MP5 ammo';
'174: Rockets';
'175: Grenades';
'176: Small medipack';
'177: Large medipack';
'178: Flares';
'179: Flare';						--Spawns when Lara throws flare away, re-pickupable
'180: Savegame crystal';
'181: Sunglasses';
'182: Portable CD Player';
'183: Direction keys';
'184: Globe';						--For indicating destinations in "Select level" dialog
'185: Pistols';
'186: Shotgun';
'187: Desert Eagle';
'188: Uzis';
'189: Harpoon gun';
'190: MP5';
'191: Rocket launcher';
'192: Grenade gun';
'193: Pistol ammo';
'194: Shotgun ammo';
'195: Desert Eagle ammo';
'196: Uzi ammo';
'197: Harpoons';
'198: MP5 ammo';
'199: Rockets';
'200: Grenades';
'201: Small medipack';
'202: Large medipack';
'203: Flares';
'204: Savegame crystal';
'205: Puzzle 1';
'206: Puzzle 2';
'207: Puzzle 3';
'208: Puzzle 4';
'209: Puzzle 1';
'210: Puzzle 2';
'211: Puzzle 3';
'212: Puzzle 4';
'213: Slot 1 empty';				 --Swaps to entity ID #217 when entity ID #209 is used
'214: Slot 2 empty';				 --Swaps to entity ID #218 when entity ID #210 is used
'215: Slot 3 empty';				 --Swaps to entity ID #219 when entity ID #211 is used
'216: Slot 4 empty';				 --Swaps to entity ID #220 when entity ID #212 is used
'217: Slot 1 full';
'218: Slot 2 full';
'219: Slot 3 full';
'220: Slot 4 full';
'221: ';
'222: ';
'223: ';
'224: Key 1';
'225: Key 2';
'226: Key 3';
'227: Key 4';
'228: Key 1';
'229: Key 2';
'230: Key 3';
'231: Key 4';
'232: Keyhole 1';					--Used with entity ID #224
'233: Keyhole 2';					--Used with entity ID #225
'234: Keyhole 3';					--Used with entity ID #226
'235: Keyhole 4';					--Used with entity ID #227
'236: Quest item 1';
'237: Quest item 2';				 --No effect on actual inventory
'238: Quest item 1';
'239: Quest item 2';				 --No effect on actual inventory
'240: Infada stone';
'241: Element 115';
'242: Eye of Isis';
'243: Ora dagger';
'244: Infada stone';
'245: Element 115';
'246: Eye of Isis';
'247: Ora dagger';
'248: ';
'249: ';
'250: ';
'251: ';
'252: ';
'253: ';
'254: ';
'255: ';
'256: ';
'257: ';
'258: ';
'259: ';
'260: ';
'261: ';
'262: ';
'263: ';
'264: ';
'265: ';
'266: ';
'267: ';
'268: ';
'269: ';
'270: ';
'271: ';
'272: Keys (sprite)';
'273: Keys (sprite)';
'274: ';
'275: ';
'276: Infada stone';
'277: Element 115';
'278: Eye of Isis';
'279: Ora dagger';
'280: ';
'281: ';
'282: Fire-breathing dragon statue';
'283: ';
'284: ';
'285: Unknown visible 285';
'286: ';
'287: Tyrannosaur';
'288: Raptor';
'289: ';
'290: ';
'291: Laser sweeper';
'292: Electrified Field';
'293: ';
'294: Shadow sprit';				 --Applied as a blob shadow with subtractive blending
'295: Detonator switch box';
'296: Misc sprites';				 --Sprites for different purposes merged into single object
'297: Bubble';
'298: ';
'299: Glow';
'300: Gunflare';
'301: Gunflare (MP5)';
'302: ';
'303: ';
'304: Look At item';
'305: Waterfall mist';
'306: Harpoon (single)';
'307: ';
'308: ';
'309: Rocket (single)';
'310: Harpoon (single)';
'311: Grenade (single)';
'312: Big missile';
'313: Smoke';
'314: Movable Boom';
'315: Lara skin';
'316: Glow 2';
'317: Unknown visible 317';
'318: Alarm light';
'319: Light';
'320: ';
'321: Light 2';
'322: Pulsating Light';
'323: ';
'324: Red Light';
'325: Green Light';
'326: Blue Light';
'327: Light 3';
'328: Light 4';
'329: ';
'330: Fire';
'331: Alternate Fire';
'332: Alternate Fire 2';
'333: Fire 2';
'334: Smoke 2';
'335: Smoke 3';
'336: Smoke 4';
'337: Greenish Smoke';
'338: Pirahnas';
'339: Fish';
'340: ';
'341: ';
'342: ';
'343: ';
'344: ';
'345: ';
'346: ';
'347: Bat swarm';
'348: ';
'349: Animating 1';
'350: Animating 2';
'351: Animating 3';
'352: Animating 4';
'353: Animating 5';
'354: Animating 6';
'355: Skybox';
'356: Font graphics';
'357: Doorbell';					 --Produces no actual sound
'358: Unknown id 358';
'359: ';
'360: Winston';
'361: Winston in camo suit';		 --Deactivates entity ID #360 when triggered
'362: Timer font graphics';		  --Used in Assault Course and racetrack
'363: ';
'364: ';
'365: Earthquake';
'366: Yellow shell casing';		  --Spawned when firing pistols, uzis, MP5 or Desert Eagle
'367: Red shell casing';			 --Spawned when firing shotgun
'368: ';
'369: ';
'370: Tinnos light shaft';
'371: ';
'372: ';
'373: Electrical switch box';
	}					   --needs add
	LIVENT = 0x098A58
	NUMOFENT = 0x098A24	 --0x09B824
	OFFSET_FIRST_ENTITY = 0x099B10
	OFFSET_FIRST_ENTITY_ID = 0xC
	OFFSET_FIRST_ENTITY_X = 0x40
	OFFSET_FIRST_ENTITY_Y = 0x44
	OFFSET_FIRST_ENTITY_Z = 0x48
	OFFSET_FIRST_ENTITY_ROOM = 0x18
	SIZOFENT = 0x88
	
--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0			 --needs add
	CAMERAY = 0			 --needs add
	CAMERAZ = 0			 --needs add
	CAMERAROOM = 0		  --needs add
	CAM_ANGLE = 0		   --needs add
	
	CAMUPFUN = 0			--needs add

--Other:
	OFFSET_ACT = 0x09B81C
	OFFSET_TRIGGER = 0x099B14
	SPRINT = 0x3
end
----------------------------------------------------------------------------------------------TR4----------------------------------------------------------------------------------------------------------
-- Tomb Raider: IV (1999)
-- Tomb Raider: The Last Revelation
-- First release date: November 1999
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='9415F6B7' then	--TR4 (SLUS_008.85) (USA)

--Basic informations:
	POINTER_LARA = 0xA8000
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0x0A8570
	OFFSET_TIMER = 0xA7FC0
	FLIPMAP = 0			 --needs add
	ORLEV = 0x0A7F70

--Entities:
	OFFSET_ENTITY = 0x0AB638
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';
'1: Pistols animation';
'2: Uzi animation';
'3: Shotgun animation';
'4: Crossbow animation';
'5: Grenade Gun animation';
'6: Sixshooter animation';
'7: Flare animation';
'8: Lara Skin';
'9: Lara Skin Joints';
'10: Lara Scream';
'11: Lara Crossbow Laser';
'12: Lara Revolver Laser';
'13: Lara Holsters';
'14: Lara Holsters Pistols';
'15: Lara Holsters Uzis';
'16: Lara Holsters Sixshooter';
'17: Lara Speech Head 1';
'18: Lara Speech Head 2';
'19: Lara Speech Head 3';
'20: Lara Speech Head 4';
'21: Actor 1 Speech Head1';
'22: Actor 1 Speech Head2';
'23: Actor 2 Speech Head1';
'24: Actor 2 Speech Head2';
'25: Lara Water Mesh';
'26: Lara Petrol Mesh';
'27: Lara Dirt Mesh';
'28: Crowbar animation';
'29: Torch animation';
'30: Hair';
'31: Motorbike';
'32: Jeep';
'33: Vehicle Extra';
'34: Enemy Jeep';
'35: Skeleton';
'36: Skeleton Mip';
'37: Guide';
'38: Guide Mip';
'39: Von Croy';
'40: Von Croy Mip';
'41: Baddy 1';
'42: Baddy 1 Mip';
'43: Baddy 2';
'44: Baddy 2 Mip';
'45: Setha';
'46: Setha Mip';
'47: Mummy';
'48: Mummy Mip';
'49: Sphinx';
'50: Sphinx Mip';
'51: Crocodile';
'52: Crocodile Mip';
'53: Horseman';
'54: Horseman Mip';
'55: Scorpion';
'56: Scorpion Mip';
'57: Jean Yves';
'58: Jean Yves Mip';
'59: Troops';
'60: Troops Mip';
'61: Knights Templar';
'62: Knights Templar Mip';
'63: Mutant';
'64: Mutant Mip';
'65: Horse';
'66: Horse Mip';
'67: Baboon Normal';
'68: Baboon Normal Mip';
'69: Baboon Inv';
'70: Baboon Inv Mip';
'71: Baboon Silent';
'72: Baboon Silent Mip';
'73: Wild Boar';
'74: Wild Boar Mip';
'75: Harpy';
'76: Harpy Mip';
'77: Demigod 1';
'78: Demigod 1 Mip';
'79: Demigod 2';
'80: Demigod 2 Mip';
'81: Demigod 3';
'82: Demigod 3 Mip';
'83: Little Beetle';
'84: Big Beetle';
'85: Big Beetle Mip';
'86: Wraith 1';
'87: Wraith 2';
'88: Wraith 3';
'89: Wraith 4';
'90: Bat';
'91: Dog';
'92: Dog Mip';
'93: Hammerhead';
'94: Hammerhead Mip';
'95: Sas';
'96: Sas Mip';
'97: Sas Dying';
'98: Sas Dying Mip';
'99: Sas Captain';
'100: Sas Captain Mip';
'101: Sas Drag Bloke';
'102: Ahmet';
'103: Ahmet Mip';
'104: Lara Double';
'105: Lara Double Mip';
'106: Small Scorpion';
'107: Locust Emitter';
'108: Game Piece 1';
'109: Game Piece 2';
'110: Game Piece 3';
'111: Enemy Piece';
'112: Wheel Of Fortune"';
'113: Scales';
'114: Darts';
'115: Dart Emitter';
'116: Homing Dart Emitter';
'117: Falling Ceiling';
'118: Falling Block';
'119: Falling Block 2';
'120: Smashable Bike Wall';
'121: Smashable Bike Floor';
'122: Trapdoor 1';
'123: Trapdoor 2';
'124: Trapdoor 3';
'125: Floor Trapdoor 1';
'126: Floor Trapdoor 2';
'127: Ceiling Trapdoor 1';
'128: Ceiling Trapdoor 2';
'129: Scaling Trapdoor';
'130: Rollingball';
'131: Spikey Floor';
'132: Teeth Spikes';
'133: Joby Spikes';
'134: Slicer Dicer';
'135: Chain';
'136: Plough';
'137: Stargate';
'138: Hammer';
'139: Burning Floor';
'140: Cog';
'141: Spikeball';
'142: Flame';
'143: Flame Emitter';
'144: Flame Emitter 2';
'145: Flame Emitter 3';
'146: Rope';
'147: Firerope';
'148: Polerope';
'149: Oneblock Platform';
'150: Twoblock Platform';
'151: Raising Block 1';
'152: Raising Block 2';
'153: Expanding Platform';
'154: Squishy Block 1';
'155: Squishy Block 2';
'156: Pushable Object 1';
'157: Pushable Object 2';
'158: Pushable Object 3';
'159: Pushable Object 4';
'160: Pushable Object 5';
'161: Tripwire';
'162: Sentry Gun';
'163: Mine';
'164: Mapper';
'165: Obelisk';
'166: Floor 4 blade';
'167: Roof 4blade';
'168: Bird Blade';
'169: Catwalk Blade';
'170: Moving Blade';
'171: Plinth Blade';
'172: Seth Blade';
'173: Lightning Conductor';
'174: Element Puzzle';
'175: Puzzle Item 1';
'176: Puzzle Item 2';
'177: Puzzle Item 3';
'178: Puzzle Item 4';
'179: Puzzle Item 5';
'180: Puzzle Item 6';
'181: Puzzle Item 7';
'182: Puzzle Item 8';
'183: Puzzle Item 9';
'184: Puzzle Item 10';
'185: Puzzle Item 11';
'186: Puzzle Item 12';
'187: Puzzle Item 1 Combo 1" ';
'188: Puzzle Item 1 Combo 2';
'189: Puzzle Item 2 Combo 1';
'190: Puzzle Item 2 Combo 2';
'191: Puzzle Item 3 Combo 1';
'192: Puzzle Item 3 Combo 2';
'193: Puzzle Item 4 Combo 1';
'194: Puzzle Item 4 Combo 2';
'195: Puzzle Item 5 Combo 1';
'196: Puzzle Item 5 Combo 2';
'197: Puzzle Item 6 Combo 1';
'198: Puzzle Item 6 Combo 2';
'199: Puzzle Item 7 Combo 1';
'200: Puzzle Item 7 Combo 2';
'201: Puzzle Item 8 Combo 1';
'202: Puzzle Item8 Combo 2';
'203: Key Item 1';
'204: Key Item 2';
'205: Key Item 3';
'206: Key Item 4';
'207: Key Item 5';
'208: Key Item 6';
'209: Key Item 7';
'210: Key Item 8';
'211: Key Item 9';
'212: Key Item 10';
'213: Key Item 11';
'214: Key Item 12';
'215: Key Item 1 Combo 1';
'216: Key Item 1 Combo 2';
'217: Key Item 2 Combo 1';
'218: Key Item 2 Combo 2';
'219: Key Item 3 Combo 1';
'220: Key Item 3 Combo 2';
'221: Key Item 4 Combo 1';
'222: Key Item 4 Combo 2';
'223: Key Item 5 Combo 1';
'224: Key Item 5 Combo 2';
'225: Key Item 6 Combo 1';
'226: Key Item 6 Combo 2';
'227: Key Item 7 Combo 1';
'228: Key Item 7 Combo 2';
'229: Key Item 8 Combo 1';
'230: Key Item 8 Combo 2';
'231: Pickup Item 1';
'232: Pickup Item 2';
'233: Pickup Item 3';
'234: Pickup Item 4';
'235: Pickup Item 1 Combo 1';
'236: Pickup Item 1 Combo 2';
'237: Pickup Item 2 Combo 1';
'238: Pickup Item 2 Combo 2';
'239: Pickup Item 3 Combo 1';
'240: Pickup Item 3 Combo 2';
'241: Pickup Item 4 Combo 1';
'242: Pickup Item 4 Combo 2';
'243: Examine 1';
'244: Examine 2';
'245: Examine 3';
'246: Crowbar Item';
'247: Burning Torch Item';
'248: Clockwork Beetle';
'249: Clockwork Beetle Combo 1';
'250: Clockwork Beetle Combo 2';
'251: Mine Detector';
'252: Quest Item 1';
'253: Quest Item 2';
'254: Quest Item 3';
'255: Quest Item 4';
'256: Quest Item 5';
'257: Quest Item 6';
'258: Map';
'259: Secret Map';
'260: Puzzle Hole 1';
'261: Puzzle Hole 2';
'262: Puzzle Hole 3';
'263: Puzzle Hole 4';
'264: Puzzle Hole 5';
'265: Puzzle Hole 6';
'266: Puzzle Hole 7';
'267: Puzzle Hole 8';
'268: Puzzle Hole 9';
'269: Puzzle Hole 10';
'270: Puzzle Hole 11';
'271: Puzzle Hole 12';
'272: Puzzle Done 1';
'273: Puzzle Done 2';
'274: Puzzle Done 3';
'275: Puzzle Done 4';
'276: Puzzle Done 5';
'277: Puzzle Done 6';
'278: Puzzle Done 7';
'279: Puzzle Done 8';
'280: Puzzle Done 9';
'281: Puzzle Done 10';
'282: Puzzle Done 11';
'283: Puzzle Done 12';
'284: Key Hole 1';
'285: Key Hole 2';
'286: Key Hole 3';
'287: Key Hole 4';
'288: Key Hole 5';
'289: Key Hole 6';
'290: Key Hole 7';
'291: Key Hole 8';
'292: Key Hole 9';
'293: Key Hole 10';
'294: Key Hole 11';
'295: Key Hole 12';
'296: Waterskin 1 Empty';
'297: Waterskin 1_1';
'298: Waterskin 1_2';
'299: Waterskin 1_3';
'300: Waterskin 2 Empty';
'301: Waterskin 2_1';
'302: Waterskin 2_2';
'303: Waterskin 2_3';
'304: Waterskin 2_4';
'305: Waterskin 2_5';
'306: Switch Type 1';
'307: Switch Type 2';
'308: Switch Type 3';
'309: Switch Type 4';
'310: Switch Type 5';
'311: Switch Type 6';
'312: Switch Type 7';
'313: Switch Type 8';
'314: Underwater Switch 1';
'315: Underwater Switch 2';
'316: Turn Switch';
'317: Cog Switch';
'318: Lever Switch';
'319: Jump Switch';
'320: Crowbar Switch';
'321: Pulley';
'322: Door Type 1';
'323: Door Type 2';
'324: Door Type 3';
'325: Door Type 4';
'326: Door Type 5';
'327: Door Type 6';
'328: Door Type 7';
'329: Door Type 8';
'330: Pushpull Door 1';
'331: Pushpull Door 2';
'332: Kick Door 1';
'333: Kick Door 2';
'334: Underwater Door';
'335: Double Doors';
'336: Bridge Flat';
'337: Bridge Tilt 1';
'338: Bridge Tilt 2';
'339: Sarcophagus';
'340: Sequence Door 1';
'341: Sequence Switch 1';
'342: Sequence Switch 2';
'343: Sequence Switch 3';
'344: Sarcophagus Cut';
'345: Horus Statue';
'346: God Head';
'347: Seth Door';
'348: Statue Plinth';
'349: Pistols Item';
'350: Pistols Ammo Item';
'351: Uzi Item';
'352: Uzi Ammo Item';
'353: Shotgun Item';
'354: Shotgun Ammo 1 Item';
'355: Shotgun Ammo 2 Item"';
'356: Crossbow Item';
'357: Crossbow Ammo 1 Item';
'358: Crossbow Ammo 2 Item';
'359: Crossbow Ammo 3 Item';
'360: Crossbow Bolt';
'361: Grenade Gun Item';
'362: Grenade Gun Ammo 1 Item';
'363: Grenade Gun Ammo 2 Item';
'364: Grenade Gun Ammo 3 Item"';
'365: Grenade';
'366: Sixshooter Item';
'367: Sixshooter Ammo Item';
'368: Big medipack Item';
'369: Small medipack Item';
'370: Lasersight Item';
'371: Binoculars Item';
'372: Flare Item';
'373: Flare Inv Item';
'374: Diary Item';
'375: Compass Item';
'376: Memcard Load Inv Item';
'377: Memcard Save Inv Item';
'378: Pc Load Inv Item';
'379: Pc Save Inv Item';
'380: Smoke Emitter White';
'381: Smoke Emitter Black';
'382: Steam Emitter';
'383: Earthquake';
'384: Bubbles';
'385: Waterfallmist';
'386: Gunshell';
'387: Shotgunshell';
'388: Gun Flash';
'389: Butterfly';
'390: Sprinkler';
'391: Red Light';
'392: Green Light';
'393: Blue Light';
'394: Amber Light';
'395: White Light';
'396: Blinking Light';
'397: Lens Flare';
'398: AI Guard';
'399: AI Ambush';
'400: AI Patrol 1';
'401: AI Modify';
'402: AI Follow';
'403: AI Patrol 2';
'404: AI X1';
'405: AI X2';
'406: Lara Start Pos';
'407: Kill All Triggers';
'408: Trigger Triggerer';
'409: Smash Object 1';
'410: Smash Object 2';
'411: Smash Object 3';
'412: Smash Object 4';
'413: Smash Object 5';
'414: Smash Object 6';
'415: Smash Object 7';
'416: Smash Object 8';
'417: Meshswap 1';
'418: Meshswap 2';
'419: Meshswap 3';
'420: Death Slide';
'421: Body Part';
'422: Camera Target';
'423: Waterfall 1';
'424: Waterfall 2';
'425: Waterfall 3';
'426: Planet Effect';
'427: Animating 1';
'428: Animating 1 Mip';
'429: Animating 2';
'430: Animating 2 Mip';
'431: Animating 3';
'432: Animating 3 Mip';
'433: Animating 4';
'434: Animating 4 Mip';
'435: Animating 5';
'436: Animating 5 Mip';
'437: Animating 6';
'438: Animating 6 Mip';
'439: Animating 7';
'440: Animating 7 Mip';
'441: Animating 8';
'442: Animating 8 Mip';
'443: Animating 9';
'444: Animating 9 Mip';
'445: Animating 10';
'446: Animating 10 Mip';
'447: Animating 11';
'448: Animating 11 Mip';
'449: Animating 12';
'450: Animating 12 Mip';
'451: Animating 13';
'452: Animating 13 Mip';
'453: Animating 14';
'454: Animating 14 Mip';
'455: Animating 15';
'456: Animating 15 Mip';
'457: Animating 16';
'458: Animating 16 Mip';
'459: Horizon';
'460: Sky Graphics';
'461: Binocular Graphics';
'462: Target Graphics';
'463: Default sprites';
'464: Misc sprites';
'465: NG Motor Boat';
'466: NG Motor Boat Lara';
'467: NG Rubber Boat';
'468: NG Rubber Boat Lara';
'469: NG Motorbike Lara';
'470: NG Font Graphics';
'471: NG Parallel Bars';
'472: NG Panel Border';
'473: NG Panel Middle';
'474: NG Panel Corner';
'475: NG Panel Diagonal';
'476: NG Panel Strip';
'477: NG Panel Half Border 1';
'478: NG Panel Half Border 2';
'479: NG Panel Middle Corner';
'480: NG Tight Rope';
'481: NG Laser Head';
'482: NG Laser Head Base';
'483: NG Laser Head Tentacle';
'484: NG Hydra';
'485: NG Hydra Missile';
'486: NG Enemy Sub Marine';
'487: NG Enemy Sub Marine Mip';
'488: NG Sub Marine Missile';
'489: NG Frog Man';
'490: NG Frog Man Harpoon';
'491: NG Fish Emitter';
'492: NG Kayak';
'493: NG Kayak Lara';
'494: NG Custom sprites';
'495: NG Bridge Tilt 3';
'496: NG Bridge Tilt 4';
'497: NG Bridge Custom';
'498: NG Robot Cleaner';
'499: NG Robot Star Wars';
'500: NG Mech Warrior';
'501: NG Mech Warrior Lara';
'502: NG Uw Propulsor';
'503: NG Uw Propulsor Lara';
'504: NG Mine Cart';
'505: NG Mine Cart Lara';
'506: NG New Slot 5';
'507: NG New Slot 6';
'508: NG New Slot 7';
'509: NG New Slot 8';
'510: NG New Slot 9';
'511: NG New Slot 10';
'512: NG New Slot 11';
'513: NG New Slot 12';
'514: NG New Slot 13';
'515: NG New Slot 14';
'516: NG New Slot 15';
'517: NG New Slot 16';
'518: NG New Slot 17';
'519: NG New Slot 18';
	}					   --needs add
	LIVENT = 0x0A72DC
	NUMOFENT = 0			--needs add
	OFFSET_FIRST_ENTITY = 0x0A812C
	OFFSET_FIRST_ENTITY_ID = 0		 --needs add
	OFFSET_FIRST_ENTITY_X = 0		  --needs add
	OFFSET_FIRST_ENTITY_Y = 0		  --needs add
	OFFSET_FIRST_ENTITY_Z = 0		  --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0		--needs add
	SIZOFENT = 0			--needs add

--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0x0A8358
	CAMERAY = 0x0A835C
	CAMERAZ = 0x0A8360
	CAMERAROOM = 0x0A8364
	CAM_ANGLE = 0		   --needs add
	
	CAMUPFUN = 0x0A801C

--Other:
	OFFSET_ACT = 0x0A95B4
	OFFSET_TRIGGER = 0x0A8130
	SPRINT = 0x4
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='9B234861' then	--TR4 (SLES_022.39) (FR)

--Basic informations:
	POINTER_LARA = 0x0A7D1C
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0x0A828C
	OFFSET_TIMER = 0x0A7CDC
	FLIPMAP = 0			 --needs add
	ORLEV = 0			   --needs add

--Entities:
	OFFSET_ENTITY = 0x0AB354
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';
'1: Pistols animation';
'2: Uzi animation';
'3: Shotgun animation';
'4: Crossbow animation';
'5: Grenade Gun animation';
'6: Sixshooter animation';
'7: Flare animation';
'8: Lara Skin';
'9: Lara Skin Joints';
'10: Lara Scream';
'11: Lara Crossbow Laser';
'12: Lara Revolver Laser';
'13: Lara Holsters';
'14: Lara Holsters Pistols';
'15: Lara Holsters Uzis';
'16: Lara Holsters Sixshooter';
'17: Lara Speech Head 1';
'18: Lara Speech Head 2';
'19: Lara Speech Head 3';
'20: Lara Speech Head 4';
'21: Actor 1 Speech Head1';
'22: Actor 1 Speech Head2';
'23: Actor 2 Speech Head1';
'24: Actor 2 Speech Head2';
'25: Lara Water Mesh';
'26: Lara Petrol Mesh';
'27: Lara Dirt Mesh';
'28: Crowbar animation';
'29: Torch animation';
'30: Hair';
'31: Motorbike';
'32: Jeep';
'33: Vehicle Extra';
'34: Enemy Jeep';
'35: Skeleton';
'36: Skeleton Mip';
'37: Guide';
'38: Guide Mip';
'39: Von Croy';
'40: Von Croy Mip';
'41: Baddy 1';
'42: Baddy 1 Mip';
'43: Baddy 2';
'44: Baddy 2 Mip';
'45: Setha';
'46: Setha Mip';
'47: Mummy';
'48: Mummy Mip';
'49: Sphinx';
'50: Sphinx Mip';
'51: Crocodile';
'52: Crocodile Mip';
'53: Horseman';
'54: Horseman Mip';
'55: Scorpion';
'56: Scorpion Mip';
'57: Jean Yves';
'58: Jean Yves Mip';
'59: Troops';
'60: Troops Mip';
'61: Knights Templar';
'62: Knights Templar Mip';
'63: Mutant';
'64: Mutant Mip';
'65: Horse';
'66: Horse Mip';
'67: Baboon Normal';
'68: Baboon Normal Mip';
'69: Baboon Inv';
'70: Baboon Inv Mip';
'71: Baboon Silent';
'72: Baboon Silent Mip';
'73: Wild Boar';
'74: Wild Boar Mip';
'75: Harpy';
'76: Harpy Mip';
'77: Demigod 1';
'78: Demigod 1 Mip';
'79: Demigod 2';
'80: Demigod 2 Mip';
'81: Demigod 3';
'82: Demigod 3 Mip';
'83: Little Beetle';
'84: Big Beetle';
'85: Big Beetle Mip';
'86: Wraith 1';
'87: Wraith 2';
'88: Wraith 3';
'89: Wraith 4';
'90: Bat';
'91: Dog';
'92: Dog Mip';
'93: Hammerhead';
'94: Hammerhead Mip';
'95: Sas';
'96: Sas Mip';
'97: Sas Dying';
'98: Sas Dying Mip';
'99: Sas Captain';
'100: Sas Captain Mip';
'101: Sas Drag Bloke';
'102: Ahmet';
'103: Ahmet Mip';
'104: Lara Double';
'105: Lara Double Mip';
'106: Small Scorpion';
'107: Locust Emitter';
'108: Game Piece 1';
'109: Game Piece 2';
'110: Game Piece 3';
'111: Enemy Piece';
'112: Wheel Of Fortune"';
'113: Scales';
'114: Darts';
'115: Dart Emitter';
'116: Homing Dart Emitter';
'117: Falling Ceiling';
'118: Falling Block';
'119: Falling Block 2';
'120: Smashable Bike Wall';
'121: Smashable Bike Floor';
'122: Trapdoor 1';
'123: Trapdoor 2';
'124: Trapdoor 3';
'125: Floor Trapdoor 1';
'126: Floor Trapdoor 2';
'127: Ceiling Trapdoor 1';
'128: Ceiling Trapdoor 2';
'129: Scaling Trapdoor';
'130: Rollingball';
'131: Spikey Floor';
'132: Teeth Spikes';
'133: Joby Spikes';
'134: Slicer Dicer';
'135: Chain';
'136: Plough';
'137: Stargate';
'138: Hammer';
'139: Burning Floor';
'140: Cog';
'141: Spikeball';
'142: Flame';
'143: Flame Emitter';
'144: Flame Emitter 2';
'145: Flame Emitter 3';
'146: Rope';
'147: Firerope';
'148: Polerope';
'149: Oneblock Platform';
'150: Twoblock Platform';
'151: Raising Block 1';
'152: Raising Block 2';
'153: Expanding Platform';
'154: Squishy Block 1';
'155: Squishy Block 2';
'156: Pushable Object 1';
'157: Pushable Object 2';
'158: Pushable Object 3';
'159: Pushable Object 4';
'160: Pushable Object 5';
'161: Tripwire';
'162: Sentry Gun';
'163: Mine';
'164: Mapper';
'165: Obelisk';
'166: Floor 4 blade';
'167: Roof 4blade';
'168: Bird Blade';
'169: Catwalk Blade';
'170: Moving Blade';
'171: Plinth Blade';
'172: Seth Blade';
'173: Lightning Conductor';
'174: Element Puzzle';
'175: Puzzle Item 1';
'176: Puzzle Item 2';
'177: Puzzle Item 3';
'178: Puzzle Item 4';
'179: Puzzle Item 5';
'180: Puzzle Item 6';
'181: Puzzle Item 7';
'182: Puzzle Item 8';
'183: Puzzle Item 9';
'184: Puzzle Item 10';
'185: Puzzle Item 11';
'186: Puzzle Item 12';
'187: Puzzle Item 1 Combo 1" ';
'188: Puzzle Item 1 Combo 2';
'189: Puzzle Item 2 Combo 1';
'190: Puzzle Item 2 Combo 2';
'191: Puzzle Item 3 Combo 1';
'192: Puzzle Item 3 Combo 2';
'193: Puzzle Item 4 Combo 1';
'194: Puzzle Item 4 Combo 2';
'195: Puzzle Item 5 Combo 1';
'196: Puzzle Item 5 Combo 2';
'197: Puzzle Item 6 Combo 1';
'198: Puzzle Item 6 Combo 2';
'199: Puzzle Item 7 Combo 1';
'200: Puzzle Item 7 Combo 2';
'201: Puzzle Item 8 Combo 1';
'202: Puzzle Item8 Combo 2';
'203: Key Item 1';
'204: Key Item 2';
'205: Key Item 3';
'206: Key Item 4';
'207: Key Item 5';
'208: Key Item 6';
'209: Key Item 7';
'210: Key Item 8';
'211: Key Item 9';
'212: Key Item 10';
'213: Key Item 11';
'214: Key Item 12';
'215: Key Item 1 Combo 1';
'216: Key Item 1 Combo 2';
'217: Key Item 2 Combo 1';
'218: Key Item 2 Combo 2';
'219: Key Item 3 Combo 1';
'220: Key Item 3 Combo 2';
'221: Key Item 4 Combo 1';
'222: Key Item 4 Combo 2';
'223: Key Item 5 Combo 1';
'224: Key Item 5 Combo 2';
'225: Key Item 6 Combo 1';
'226: Key Item 6 Combo 2';
'227: Key Item 7 Combo 1';
'228: Key Item 7 Combo 2';
'229: Key Item 8 Combo 1';
'230: Key Item 8 Combo 2';
'231: Pickup Item 1';
'232: Pickup Item 2';
'233: Pickup Item 3';
'234: Pickup Item 4';
'235: Pickup Item 1 Combo 1';
'236: Pickup Item 1 Combo 2';
'237: Pickup Item 2 Combo 1';
'238: Pickup Item 2 Combo 2';
'239: Pickup Item 3 Combo 1';
'240: Pickup Item 3 Combo 2';
'241: Pickup Item 4 Combo 1';
'242: Pickup Item 4 Combo 2';
'243: Examine 1';
'244: Examine 2';
'245: Examine 3';
'246: Crowbar Item';
'247: Burning Torch Item';
'248: Clockwork Beetle';
'249: Clockwork Beetle Combo 1';
'250: Clockwork Beetle Combo 2';
'251: Mine Detector';
'252: Quest Item 1';
'253: Quest Item 2';
'254: Quest Item 3';
'255: Quest Item 4';
'256: Quest Item 5';
'257: Quest Item 6';
'258: Map';
'259: Secret Map';
'260: Puzzle Hole 1';
'261: Puzzle Hole 2';
'262: Puzzle Hole 3';
'263: Puzzle Hole 4';
'264: Puzzle Hole 5';
'265: Puzzle Hole 6';
'266: Puzzle Hole 7';
'267: Puzzle Hole 8';
'268: Puzzle Hole 9';
'269: Puzzle Hole 10';
'270: Puzzle Hole 11';
'271: Puzzle Hole 12';
'272: Puzzle Done 1';
'273: Puzzle Done 2';
'274: Puzzle Done 3';
'275: Puzzle Done 4';
'276: Puzzle Done 5';
'277: Puzzle Done 6';
'278: Puzzle Done 7';
'279: Puzzle Done 8';
'280: Puzzle Done 9';
'281: Puzzle Done 10';
'282: Puzzle Done 11';
'283: Puzzle Done 12';
'284: Key Hole 1';
'285: Key Hole 2';
'286: Key Hole 3';
'287: Key Hole 4';
'288: Key Hole 5';
'289: Key Hole 6';
'290: Key Hole 7';
'291: Key Hole 8';
'292: Key Hole 9';
'293: Key Hole 10';
'294: Key Hole 11';
'295: Key Hole 12';
'296: Waterskin 1 Empty';
'297: Waterskin 1_1';
'298: Waterskin 1_2';
'299: Waterskin 1_3';
'300: Waterskin 2 Empty';
'301: Waterskin 2_1';
'302: Waterskin 2_2';
'303: Waterskin 2_3';
'304: Waterskin 2_4';
'305: Waterskin 2_5';
'306: Switch Type 1';
'307: Switch Type 2';
'308: Switch Type 3';
'309: Switch Type 4';
'310: Switch Type 5';
'311: Switch Type 6';
'312: Switch Type 7';
'313: Switch Type 8';
'314: Underwater Switch 1';
'315: Underwater Switch 2';
'316: Turn Switch';
'317: Cog Switch';
'318: Lever Switch';
'319: Jump Switch';
'320: Crowbar Switch';
'321: Pulley';
'322: Door Type 1';
'323: Door Type 2';
'324: Door Type 3';
'325: Door Type 4';
'326: Door Type 5';
'327: Door Type 6';
'328: Door Type 7';
'329: Door Type 8';
'330: Pushpull Door 1';
'331: Pushpull Door 2';
'332: Kick Door 1';
'333: Kick Door 2';
'334: Underwater Door';
'335: Double Doors';
'336: Bridge Flat';
'337: Bridge Tilt 1';
'338: Bridge Tilt 2';
'339: Sarcophagus';
'340: Sequence Door 1';
'341: Sequence Switch 1';
'342: Sequence Switch 2';
'343: Sequence Switch 3';
'344: Sarcophagus Cut';
'345: Horus Statue';
'346: God Head';
'347: Seth Door';
'348: Statue Plinth';
'349: Pistols Item';
'350: Pistols Ammo Item';
'351: Uzi Item';
'352: Uzi Ammo Item';
'353: Shotgun Item';
'354: Shotgun Ammo 1 Item';
'355: Shotgun Ammo 2 Item"';
'356: Crossbow Item';
'357: Crossbow Ammo 1 Item';
'358: Crossbow Ammo 2 Item';
'359: Crossbow Ammo 3 Item';
'360: Crossbow Bolt';
'361: Grenade Gun Item';
'362: Grenade Gun Ammo 1 Item';
'363: Grenade Gun Ammo 2 Item';
'364: Grenade Gun Ammo 3 Item"';
'365: Grenade';
'366: Sixshooter Item';
'367: Sixshooter Ammo Item';
'368: Big medipack Item';
'369: Small medipack Item';
'370: Lasersight Item';
'371: Binoculars Item';
'372: Flare Item';
'373: Flare Inv Item';
'374: Diary Item';
'375: Compass Item';
'376: Memcard Load Inv Item';
'377: Memcard Save Inv Item';
'378: Pc Load Inv Item';
'379: Pc Save Inv Item';
'380: Smoke Emitter White';
'381: Smoke Emitter Black';
'382: Steam Emitter';
'383: Earthquake';
'384: Bubbles';
'385: Waterfallmist';
'386: Gunshell';
'387: Shotgunshell';
'388: Gun Flash';
'389: Butterfly';
'390: Sprinkler';
'391: Red Light';
'392: Green Light';
'393: Blue Light';
'394: Amber Light';
'395: White Light';
'396: Blinking Light';
'397: Lens Flare';
'398: AI Guard';
'399: AI Ambush';
'400: AI Patrol 1';
'401: AI Modify';
'402: AI Follow';
'403: AI Patrol 2';
'404: AI X1';
'405: AI X2';
'406: Lara Start Pos';
'407: Kill All Triggers';
'408: Trigger Triggerer';
'409: Smash Object 1';
'410: Smash Object 2';
'411: Smash Object 3';
'412: Smash Object 4';
'413: Smash Object 5';
'414: Smash Object 6';
'415: Smash Object 7';
'416: Smash Object 8';
'417: Meshswap 1';
'418: Meshswap 2';
'419: Meshswap 3';
'420: Death Slide';
'421: Body Part';
'422: Camera Target';
'423: Waterfall 1';
'424: Waterfall 2';
'425: Waterfall 3';
'426: Planet Effect';
'427: Animating 1';
'428: Animating 1 Mip';
'429: Animating 2';
'430: Animating 2 Mip';
'431: Animating 3';
'432: Animating 3 Mip';
'433: Animating 4';
'434: Animating 4 Mip';
'435: Animating 5';
'436: Animating 5 Mip';
'437: Animating 6';
'438: Animating 6 Mip';
'439: Animating 7';
'440: Animating 7 Mip';
'441: Animating 8';
'442: Animating 8 Mip';
'443: Animating 9';
'444: Animating 9 Mip';
'445: Animating 10';
'446: Animating 10 Mip';
'447: Animating 11';
'448: Animating 11 Mip';
'449: Animating 12';
'450: Animating 12 Mip';
'451: Animating 13';
'452: Animating 13 Mip';
'453: Animating 14';
'454: Animating 14 Mip';
'455: Animating 15';
'456: Animating 15 Mip';
'457: Animating 16';
'458: Animating 16 Mip';
'459: Horizon';
'460: Sky Graphics';
'461: Binocular Graphics';
'462: Target Graphics';
'463: Default sprites';
'464: Misc sprites';
'465: NG Motor Boat';
'466: NG Motor Boat Lara';
'467: NG Rubber Boat';
'468: NG Rubber Boat Lara';
'469: NG Motorbike Lara';
'470: NG Font Graphics';
'471: NG Parallel Bars';
'472: NG Panel Border';
'473: NG Panel Middle';
'474: NG Panel Corner';
'475: NG Panel Diagonal';
'476: NG Panel Strip';
'477: NG Panel Half Border 1';
'478: NG Panel Half Border 2';
'479: NG Panel Middle Corner';
'480: NG Tight Rope';
'481: NG Laser Head';
'482: NG Laser Head Base';
'483: NG Laser Head Tentacle';
'484: NG Hydra';
'485: NG Hydra Missile';
'486: NG Enemy Sub Marine';
'487: NG Enemy Sub Marine Mip';
'488: NG Sub Marine Missile';
'489: NG Frog Man';
'490: NG Frog Man Harpoon';
'491: NG Fish Emitter';
'492: NG Kayak';
'493: NG Kayak Lara';
'494: NG Custom sprites';
'495: NG Bridge Tilt 3';
'496: NG Bridge Tilt 4';
'497: NG Bridge Custom';
'498: NG Robot Cleaner';
'499: NG Robot Star Wars';
'500: NG Mech Warrior';
'501: NG Mech Warrior Lara';
'502: NG Uw Propulsor';
'503: NG Uw Propulsor Lara';
'504: NG Mine Cart';
'505: NG Mine Cart Lara';
'506: NG New Slot 5';
'507: NG New Slot 6';
'508: NG New Slot 7';
'509: NG New Slot 8';
'510: NG New Slot 9';
'511: NG New Slot 10';
'512: NG New Slot 11';
'513: NG New Slot 12';
'514: NG New Slot 13';
'515: NG New Slot 14';
'516: NG New Slot 15';
'517: NG New Slot 16';
'518: NG New Slot 17';
'519: NG New Slot 18';
	}					   --needs add
	LIVENT = 0x0A6FF8
	NUMOFENT = 0			--needs add
	OFFSET_FIRST_ENTITY = 0		--needs add
	OFFSET_FIRST_ENTITY_ID = 0		 --needs add
	OFFSET_FIRST_ENTITY_X = 0		  --needs add
	OFFSET_FIRST_ENTITY_Y = 0		  --needs add
	OFFSET_FIRST_ENTITY_Z = 0		  --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0		--needs add
	SIZOFENT = 0			--needs add

--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0			 --needs add
	CAMERAY = 0			 --needs add
	CAMERAZ = 0			 --needs add
	CAMERAROOM = 0		  --needs add
	CAM_ANGLE = 0		   --needs add
	
	CAMUPFUN = 0			--needs add

--Other:
	OFFSET_ACT = 0x0A92D0
	OFFSET_TRIGGER = 0		--needs add
	SPRINT = 0x4
end
----------------------------------------------------------------------------------------------TR5----------------------------------------------------------------------------------------------------------
-- Tomb Raider: V (2000)
-- Tomb Raider: Chronicles
-- First release date: November 2000

--Notes:
		--Laser Head doesn't count to living entities
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='7762BF5D' then	--TR5 (SLPM_867.79) (JAP)

--Basic informations:
	POINTER_LARA = 0x0A460C
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0			 --needs add
	OFFSET_TIMER = 0x0A45B4
	FLIPMAP = 0			 --needs add
	ORLEV = 0			   --needs add

--Entities:
	OFFSET_ENTITY = 0x0A7DFC
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';
'1: Pistol animation';
'2: Uzi animation';
'3: Shotgun animation';
'4: Grappling gun animation';
'5: HK Gun animation';
'6: Revolver animation';
'7: Flare animation';
'8: Lara skin';
'9: Lara skin joints';
'10: Lara scream';
'11: Lara grappling laser';
'12: Lara revolver laser';
'13: Lara holsters';
'14: Lara holsters pistols';
'15: Lara holsters uzis';
'16: Lara holsters revolver';
'17: Lara head 1';
'18: Lara head 2';
'19: Lara head 3';
'20: Lara head 4';
'21: Actor 1 head 1';
'22: Actor 1 head 2';
'23: Actor 2 head 1';
'24: Actor 2 head 2';
'25: Lara X-Ray';
'26: Lara X-Ray 1';
'27: Lara X-Ray 2';
'28: Crowbar animation"';
'29: Torch animation';
'30: Hair';
'31: Guard 1';
'32: Guard 1_mip';
'33: Guard 2';
'34: Guard 2_mip';
'35: Guard 3';
'36: Guard 3_mip';
'37: Guard 4';
'38: Guard 4_mip';
'39: Guard laser';
'40: Guard laser_mip';
'41: Doberman';
'42: Doberman_mip';
'43: Crow';
'44: Crow_mip';
'45: Larson';
'46: Larson_mip';
'47: Pierre';
'48: Pierre_mip';
'49: Armed baddy 1';
'50: Armed baddy 1_mip';
'51: Armed baddy 2';
'52: Armed baddy 2_mip';
'53: Guard 5';
'54: Guard 5_mip';
'55: Guard 6';
'56: Guard 6_mip';
'57: Lion';
'58: Lion_mip';
'59: Gladiator';
'60: Gladiator_mip';
'61: Roman statue';
'62: Roman statue_mip';
'63: Small dragon';
'64: Small dragon_mip';
'65: Laser head';
'66: Laser head_mip';
'67: Cyborg';
'68: Cyborg_mip';
'69: VCI worker';
'70: VCI worker_mip';
'71: Lighthing Guide';
'72: Lighthing Guide_mip';
'73: Invisible ghost';
'74: Invisible ghost_mip';
'75: Reaper';
'76: Reaper_mip';
'77: Brown beast';
'78: Brown beast_mip';
'79: Lagoon Witch';
'80: Lagoon Witch_mip';
'81: Submarine';
'82: Submarine_mip';
'83: M16 guard';
'84: M16 guard_mip';
'85: Dog';
'86: Dog_mip';
'87: The chef';
'88: Rich 1 hammer door (The chef mip)';
'89: Imp';
'90: Imp mip';
'91: Gunship';
'92: Gunship mip';
'93: Little bat';
'94: Little rat';
'95: Spider';
'96: Spider generator';
'97: Auto guns';
'98: Electricity wires';
'99: Dart';
'100: Dart emitter';
'101: Fast dart emitter';
'102: Falling ceiling';
'103: Falling block 1';
'104: Falling block 2';
'105: Crumbling floor';
'106: Trapdoor 1';
'107: Trapdoor 2';
'108: Trapdoor 3';
'109: Floor trapdoor 1';
'110: Floor trapdoor 2';
'111: Ceiling trapdoor 1';
'112: Ceiling trapdoor 2';
'113: Scaling trapdoor';
'114: Rolling ball';
'115: Rolling barrel';
'116: Spiky floor';
'117: Spikes';
'118: Ram';
'119: Rome hammer';
'120: Flame';
'121: Big flame';
'122: Torch flame';
'123: Death flame';
'124: Gas fire';
'125: Burning roots';
'126: Rope';
'127: Fire rope';
'128: Pole rope';
'129: Ventilator';
'130: Ventilator';
'131: Grappling gun target';
'132: One-block platform';
'133: Two-block platform';
'134: Raising block';
'135: Teleport (?)';
'136: Headset talk point (?)';
'137: Pushable 1';
'138: Pushable 2';
'139: Pushable 3';
'140: Pushable 4';
'141: Pushable 5';
'142: Robot arm';
'143: Death slide';
'144: Rocket item';
'145: Chaff flare';
'146: Satchel bomb';
'147: Electric fence';
'148: Lift';
'149: Bomb';
'150: Iris lightning"';
'151: Monitor';
'152: Security screen';
'153: Motion sensors';
'154: Tight rope';
'155: Parallel bars';
'156: X-ray controller (dummy item)';
'157: Cutscene rope';
'158: Flat window';
'159: Generic slot 1';
'160: Gas emitter';
'161: Generic slot 2';
'162: Moving laser';
'163: Imp rock';
'164: Cupboard';
'165: Cupboard_mip';
'166: Cupboard';
'167: Cupboard_mip';
'168: Cupboard';
'169: Cupboard_mip';
'170: Suitcase';
'171: Suitcase_mip';
'172: Puzzle 1';
'173: Puzzle 2';
'174: Puzzle 3';
'175: Puzzle 4';
'176: Puzzle 5';
'177: Puzzle 6';
'178: Puzzle 7';
'179: Puzzle 8';
'180: Puzzle 1 Combo 1';
'181: Puzzle 1 Combo 2';
'182: Puzzle 2 Combo 1';
'183: Puzzle 2 Combo 2';
'184: Puzzle 3 Combo 1';
'185: Puzzle 3 Combo 2';
'186: Puzzle 4 Combo 1';
'187: Puzzle 4 Combo 2';
'188: Puzzle 5 Combo 1';
'189: Puzzle 5 Combo 2';
'190: Puzzle 6 Combo 1';
'191: Puzzle 6 Combo 2';
'192: Puzzle 7 Combo 1';
'193: Puzzle 7 Combo 2';
'194: Puzzle 8 Combo 1';
'195: Puzzle 8 Combo 2';
'196: Key 1';
'197: Key 2';
'198: Key 3';
'199: Key 4';
'200: Key 5';
'201: Key 6';
'202: Key 7';
'203: Key 8';
'204: Key 1 Combo 1';
'205: Key 1 Combo 2';
'206: Key 2 Combo 1';
'207: Key 2 Combo 2';
'208: Key 3 Combo 1';
'209: Key 3 Combo 2';
'210: Key 4 Combo 1';
'211: Key 4 Combo 2';
'212: Key 5 Combo 1';
'213: Key 5 Combo 2';
'214: Key 6 Combo 1';
'215: Key 6 Combo 2';
'216: Key 7 Combo 1';
'217: Key 7 Combo 2';
'218: Key 8 Combo 1';
'219: Key 8 Combo 2';
'220: Pickup item 1';
'221: Pickup item 2';
'222: Pickup item 3';
'223: Gold rose';
'224: Pickup item 1 combo 1';
'225: Pickup item 1 combo 2';
'226: Pickup item 2 combo 1';
'227: Pickup item 2 combo 2';
'228: Pickup item 3 combo 1';
'229: Pickup item 3 combo 2';
'230: Gold rose combo 1';
'231: Gold rose combo 2';
'232: Examine 1';
'233: Examine 2';
'234: Examine 3';
'235: Chloroform bottle';
'236: Chloroform cloth';
'237: Chloroform soaked cloth';
'238: Cosh';
'239: Hammer item';
'240: Crowbar item';
'241: Torch item';
'242: Slot empty 1';
'243: Slot empty 2';
'244: Slot empty 3';
'245: Slot empty 4';
'246: Slot empty 5';
'247: Slot empty 6';
'248: Slot empty 7';
'249: Slot empty 8';
'250: Slot full 1';
'251: Slot full 2';
'252: Slot full 3';
'253: Slot full 4';
'254: Slot full 5';
'255: Slot full 6';
'256: Slot full 7';
'257: Slot full 8';
'258: Lock 1';
'259: Lock 2';
'260: Lock 3';
'261: Lock 4';
'262: Lock 5';
'263: Lock 6';
'264: Lock 7';
'265: Lock 8';
'266: Switch type 1';
'267: Switch type 2';
'268: Switch type 3';
'269: Switch type 4';
'270: Switch type 5';
'271: Switch type 6';
'272: Shoot switch 1';
'273: Shoot switch 2';
'274: Airlock switch';
'275: Underwater switch 1';
'276: Underwater switch 2';
'277: Turn switch';
'278: Cog switch';
'279: Lever switch';
'280: Jump switch';
'281: Crowbar switch';
'282: Pulley';
'283: Crowdove switch';
'284: Door 1';
'285: Door 1_mip';
'286: Door 2';
'287: Door 2_mip';
'288: Door 3';
'289: Door 3_mip';
'290: Door 4';
'291: Door 4_mip';
'292: Door 5';
'293: Door 5_mip';
'294: Door 6';
'295: Door 6_mip';
'296: Door 7';
'297: Door 7_mip';
'298: Door 8';
'299: Door 8_mip';
'300: Door 9';
'301: Door 9_mip';
'302: Door 10';
'303: Door 10_mip';
'304: Door 11';
'305: Door 11_mip';
'306: Door 12';
'307: Door 12_mip';
'308: Door 13';
'309: Door 13_mip';
'310: Door 14';
'311: Door 14_mip';
'312: Lift doors 1';
'313: Lift doors 1 mip';
'314: Lift doors 2';
'315: Lift doors 2 mip';
'316: Push-pull door 1';
'317: Push-pull door 1 mip';
'318: Push-pull door 2';
'319: Push-pull door 2 mip';
'320: Kick door 1';
'321: Kick door 1 mip';
'322: Kick door 2';
'323: Kick door 2 mip';
'324: Underwater door';
'325: Underwater door mip';
'326: Double doors';
'327: Double doors mip';
'328: Sequence door';
'329: Sequence switch 1';
'330: Sequence switch 2';
'331: Sequence switch 3';
'332: Steel door';
'333: God head';
'334: Pistols';
'335: Pistol clip';
'336: Uzis';
'337: Uzi clip';
'338: Shotgun';
'339: Shotgun shells 1';
'340: Shotgun shells 2';
'341: Grappling gun';
'342: Grappling type 1';
'343: Grappling type 2';
'344: Grappling type 3';
'345: HK Gun';
'346: HK ammo';
'347: Revolver';
'348: Revolver bullets';
'349: Big Medipack';
'350: Small Medipack';
'351: Laser sight';
'352: Binoculars';
'353: Silencer';
'354: Burning flare';
'355: Flares';
'356: Timer';
'357: Load inventory';
'358: Save inventory';
'359: Disk load';
'360: Disk save';
'361: Memcard load';
'362: Memcard save';
'363: Smoke emitter white';
'364: Smoke emitter black';
'365: Steam emitter';
'366: Earthquake';
'367: Bubbles';
'368: Waterfall mist';
'369: Gun shell';
'370: Shotgun shell';
'371: Gun flash';
'372: Color light';
'373: Blinking light';
'374: Pulse light';
'375: Strobe light';
'376: Electrical light';
'377: Lens flare';
'378: AI guard';
'379: AI ambush';
'380: AI patrol 1';
'381: AI modify';
'382: AI follow';
'383: AI patrol 2';
'384: AI X1';
'385: AI X2';
'386: Lara start position';
'387: Lara position changer';
'388: Elevator';
'389: Raising cog';
'390: Laser';
'391: Steam laser';
'392: Floor laser';
'393: Kill all triggers';
'394: Trigger triggerer';
'395: High object 1';
'396: High object 2';
'397: Smash object 1';
'398: Smash object 2';
'399: Smash object 3';
'400: Smash object 4';
'401: Smash object 5';
'402: Smash object 6';
'403: Smash object 7';
'404: Smash object 8';
'405: Meshswap 1';
'406: Meshswap 2';
'407: Meshswap 3';
'408: Body part';
'409: Camera target';
'410: Waterfall 1';
'411: Waterfall 2';
'412: ';
'413: Waterfall 3';
'414: Waterfall 4';
'415: Waterfall 5';
'416: Animating 1';
'417: Animating 1_mip';
'418: Animating 2';
'419: Animating 2_mip';
'420: Animating 3';
'421: Animating 3_mip';
'422: Animating 4';
'423: Animating 4_mip';
'424: Animating 5';
'425: Animating 5_mip';
'426: Animating 6';
'427: Animating 6_mip';
'428: Animating 7';
'429: Animating 7_mip';
'430: Animating 8';
'431: Animating 8_mip';
'432: Animating 9';
'433: Animating 9_mip';
'434: Animating 10';
'435: Animating 10_mip';
'436: Animating 11';
'437: Animating 11_mip';
'438: Animating 12';
'439: Animating 12_mip';
'440: Animating 13';
'441: Animating 13_mip';
'442: Animating 14';
'443: Animating 14_mip';
'444: Animating 15';
'445: Animating 15_mip';
'446: Animating 16';
'447: Animating 16_mip';
'448: Floor 1 flat';
'449: Floor 2 flat';
'450: Floor 1 slope 1';
'451: Floor 2 slope 1';
'452: Floor 1 slope 2';
'453: Floor 2 slope 2';
'454: Horizon';
'455: Sky graphics';
'456: Binoculars graphics';
'457: Target graphics"';
'458: Default sprites';
'459: Misc sprites';
'460: ';
'461: ';
'462: ';
'463: ';
'464: ';
'465: ';
'466: ';
'467: ';
'468: ';
'469: ';
'470: ';
'471: ';
'472: ';
'473: ';
'474: ';
'475: ';
'476: ';
'477: ';
'478: ';
'479: ';
'480: ';
'481: ';
'482: ';
'483: ';
'484: TR3 Tiger';
	}					   --needs add
	LIVENT = 0			  --needs add
	NUMOFENT = 0			--needs add
	OFFSET_FIRST_ENTITY = 0		--needs add
	OFFSET_FIRST_ENTITY_ID = 0		 --needs add
	OFFSET_FIRST_ENTITY_X = 0		  --needs add
	OFFSET_FIRST_ENTITY_Y = 0		  --needs add
	OFFSET_FIRST_ENTITY_Z = 0		  --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0		--needs add
	SIZOFENT = 0			--needs add

--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0			 --needs add
	CAMERAY = 0			 --needs add
	CAMERAZ = 0			 --needs add
	CAMERAROOM = 0		  --needs add
	CAM_ANGLE = 0		   --needs add
	
	CAMUPFUN = 0			--needs add

--Other:
	OFFSET_ACT = 0		  --needs add
	OFFSET_TRIGGER = 0		--needs add
	SPRINT = 0x5
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if HASH=='EA435898' then	--TR5 (SLUS_013.11) (USA)

--Basic informations:
	POINTER_LARA = 0x0A3AA4
	OFFSET_FIX = 0
	OFFSET_POSITION = 0x40
	OFFSET_SPEEDH = 0x1E
	OFFSET_SPEEDV = 0x20
	OFFSET_ANGLE = 0x4E
	OFFSET_HEALTH = 0x22
	OFFSET_ROOM = 0x18
	OFFSET_CUR_ACT = 0xE
	OFFSET_ANIM_FRA = 0x16
	
	NUMROOM = 0			 --needs add
	OFFSET_TIMER = 0x0A3A6C
	FLIPMAP = 0			 --needs add
	ORLEV = 0x0A3A88

--Entities:
	OFFSET_ENTITY = 0x0A7284
	OFFSET_ENTITY_ID = 0xC
	OFFSET_ENTITY_HP = 0x22
	ENTITY_LIST={
'0: Lara';
'1: Pistol animation';
'2: Uzi animation';
'3: Shotgun animation';
'4: Grappling gun animation';
'5: HK Gun animation';
'6: Revolver animation';
'7: Flare animation';
'8: Lara skin';
'9: Lara skin joints';
'10: Lara scream';
'11: Lara grappling laser';
'12: Lara revolver laser';
'13: Lara holsters';
'14: Lara holsters pistols';
'15: Lara holsters uzis';
'16: Lara holsters revolver';
'17: Lara head 1';
'18: Lara head 2';
'19: Lara head 3';
'20: Lara head 4';
'21: Actor 1 head 1';
'22: Actor 1 head 2';
'23: Actor 2 head 1';
'24: Actor 2 head 2';
'25: Lara X-Ray';
'26: Lara X-Ray 1';
'27: Lara X-Ray 2';
'28: Crowbar animation"';
'29: Torch animation';
'30: Hair';
'31: Guard 1';
'32: Guard 1_mip';
'33: Guard 2';
'34: Guard 2_mip';
'35: Guard 3';
'36: Guard 3_mip';
'37: Guard 4';
'38: Guard 4_mip';
'39: Guard laser';
'40: Guard laser_mip';
'41: Doberman';
'42: Doberman_mip';
'43: Crow';
'44: Crow_mip';
'45: Larson';
'46: Larson_mip';
'47: Pierre';
'48: Pierre_mip';
'49: Armed baddy 1';
'50: Armed baddy 1_mip';
'51: Armed baddy 2';
'52: Armed baddy 2_mip';
'53: Guard 5';
'54: Guard 5_mip';
'55: Guard 6';
'56: Guard 6_mip';
'57: Lion';
'58: Lion_mip';
'59: Gladiator';
'60: Gladiator_mip';
'61: Roman statue';
'62: Roman statue_mip';
'63: Small dragon';
'64: Small dragon_mip';
'65: Laser head';
'66: Laser head_mip';
'67: Cyborg';
'68: Cyborg_mip';
'69: VCI worker';
'70: VCI worker_mip';
'71: Lighthing Guide';
'72: Lighthing Guide_mip';
'73: Invisible ghost';
'74: Invisible ghost_mip';
'75: Reaper';
'76: Reaper_mip';
'77: Brown beast';
'78: Brown beast_mip';
'79: Lagoon Witch';
'80: Lagoon Witch_mip';
'81: Submarine';
'82: Submarine_mip';
'83: M16 guard';
'84: M16 guard_mip';
'85: Dog';
'86: Dog_mip';
'87: The chef';
'88: Rich 1 hammer door (The chef mip)';
'89: Imp';
'90: Imp mip';
'91: Gunship';
'92: Gunship mip';
'93: Little bat';
'94: Little rat';
'95: Spider';
'96: Spider generator';
'97: Auto guns';
'98: Electricity wires';
'99: Dart';
'100: Dart emitter';
'101: Fast dart emitter';
'102: Falling ceiling';
'103: Falling block 1';
'104: Falling block 2';
'105: Crumbling floor';
'106: Trapdoor 1';
'107: Trapdoor 2';
'108: Trapdoor 3';
'109: Floor trapdoor 1';
'110: Floor trapdoor 2';
'111: Ceiling trapdoor 1';
'112: Ceiling trapdoor 2';
'113: Scaling trapdoor';
'114: Rolling ball';
'115: Rolling barrel';
'116: Spiky floor';
'117: Spikes';
'118: Ram';
'119: Rome hammer';
'120: Flame';
'121: Big flame';
'122: Torch flame';
'123: Death flame';
'124: Gas fire';
'125: Burning roots';
'126: Rope';
'127: Fire rope';
'128: Pole rope';
'129: Ventilator';
'130: Ventilator';
'131: Grappling gun target';
'132: One-block platform';
'133: Two-block platform';
'134: Raising block';
'135: Teleport (?)';
'136: Headset talk point (?)';
'137: Pushable 1';
'138: Pushable 2';
'139: Pushable 3';
'140: Pushable 4';
'141: Pushable 5';
'142: Robot arm';
'143: Death slide';
'144: Rocket item';
'145: Chaff flare';
'146: Satchel bomb';
'147: Electric fence';
'148: Lift';
'149: Bomb';
'150: Iris lightning"';
'151: Monitor';
'152: Security screen';
'153: Motion sensors';
'154: Tight rope';
'155: Parallel bars';
'156: X-ray controller (dummy item)';
'157: Cutscene rope';
'158: Flat window';
'159: Generic slot 1';
'160: Gas emitter';
'161: Generic slot 2';
'162: Moving laser';
'163: Imp rock';
'164: Cupboard';
'165: Cupboard_mip';
'166: Cupboard';
'167: Cupboard_mip';
'168: Cupboard';
'169: Cupboard_mip';
'170: Suitcase';
'171: Suitcase_mip';
'172: Puzzle 1';
'173: Puzzle 2';
'174: Puzzle 3';
'175: Puzzle 4';
'176: Puzzle 5';
'177: Puzzle 6';
'178: Puzzle 7';
'179: Puzzle 8';
'180: Puzzle 1 Combo 1';
'181: Puzzle 1 Combo 2';
'182: Puzzle 2 Combo 1';
'183: Puzzle 2 Combo 2';
'184: Puzzle 3 Combo 1';
'185: Puzzle 3 Combo 2';
'186: Puzzle 4 Combo 1';
'187: Puzzle 4 Combo 2';
'188: Puzzle 5 Combo 1';
'189: Puzzle 5 Combo 2';
'190: Puzzle 6 Combo 1';
'191: Puzzle 6 Combo 2';
'192: Puzzle 7 Combo 1';
'193: Puzzle 7 Combo 2';
'194: Puzzle 8 Combo 1';
'195: Puzzle 8 Combo 2';
'196: Key 1';
'197: Key 2';
'198: Key 3';
'199: Key 4';
'200: Key 5';
'201: Key 6';
'202: Key 7';
'203: Key 8';
'204: Key 1 Combo 1';
'205: Key 1 Combo 2';
'206: Key 2 Combo 1';
'207: Key 2 Combo 2';
'208: Key 3 Combo 1';
'209: Key 3 Combo 2';
'210: Key 4 Combo 1';
'211: Key 4 Combo 2';
'212: Key 5 Combo 1';
'213: Key 5 Combo 2';
'214: Key 6 Combo 1';
'215: Key 6 Combo 2';
'216: Key 7 Combo 1';
'217: Key 7 Combo 2';
'218: Key 8 Combo 1';
'219: Key 8 Combo 2';
'220: Pickup item 1';
'221: Pickup item 2';
'222: Pickup item 3';
'223: Gold rose';
'224: Pickup item 1 combo 1';
'225: Pickup item 1 combo 2';
'226: Pickup item 2 combo 1';
'227: Pickup item 2 combo 2';
'228: Pickup item 3 combo 1';
'229: Pickup item 3 combo 2';
'230: Gold rose combo 1';
'231: Gold rose combo 2';
'232: Examine 1';
'233: Examine 2';
'234: Examine 3';
'235: Chloroform bottle';
'236: Chloroform cloth';
'237: Chloroform soaked cloth';
'238: Cosh';
'239: Hammer item';
'240: Crowbar item';
'241: Torch item';
'242: Slot empty 1';
'243: Slot empty 2';
'244: Slot empty 3';
'245: Slot empty 4';
'246: Slot empty 5';
'247: Slot empty 6';
'248: Slot empty 7';
'249: Slot empty 8';
'250: Slot full 1';
'251: Slot full 2';
'252: Slot full 3';
'253: Slot full 4';
'254: Slot full 5';
'255: Slot full 6';
'256: Slot full 7';
'257: Slot full 8';
'258: Lock 1';
'259: Lock 2';
'260: Lock 3';
'261: Lock 4';
'262: Lock 5';
'263: Lock 6';
'264: Lock 7';
'265: Lock 8';
'266: Switch type 1';
'267: Switch type 2';
'268: Switch type 3';
'269: Switch type 4';
'270: Switch type 5';
'271: Switch type 6';
'272: Shoot switch 1';
'273: Shoot switch 2';
'274: Airlock switch';
'275: Underwater switch 1';
'276: Underwater switch 2';
'277: Turn switch';
'278: Cog switch';
'279: Lever switch';
'280: Jump switch';
'281: Crowbar switch';
'282: Pulley';
'283: Crowdove switch';
'284: Door 1';
'285: Door 1_mip';
'286: Door 2';
'287: Door 2_mip';
'288: Door 3';
'289: Door 3_mip';
'290: Door 4';
'291: Door 4_mip';
'292: Door 5';
'293: Door 5_mip';
'294: Door 6';
'295: Door 6_mip';
'296: Door 7';
'297: Door 7_mip';
'298: Door 8';
'299: Door 8_mip';
'300: Door 9';
'301: Door 9_mip';
'302: Door 10';
'303: Door 10_mip';
'304: Door 11';
'305: Door 11_mip';
'306: Door 12';
'307: Door 12_mip';
'308: Door 13';
'309: Door 13_mip';
'310: Door 14';
'311: Door 14_mip';
'312: Lift doors 1';
'313: Lift doors 1 mip';
'314: Lift doors 2';
'315: Lift doors 2 mip';
'316: Push-pull door 1';
'317: Push-pull door 1 mip';
'318: Push-pull door 2';
'319: Push-pull door 2 mip';
'320: Kick door 1';
'321: Kick door 1 mip';
'322: Kick door 2';
'323: Kick door 2 mip';
'324: Underwater door';
'325: Underwater door mip';
'326: Double doors';
'327: Double doors mip';
'328: Sequence door';
'329: Sequence switch 1';
'330: Sequence switch 2';
'331: Sequence switch 3';
'332: Steel door';
'333: God head';
'334: Pistols';
'335: Pistol clip';
'336: Uzis';
'337: Uzi clip';
'338: Shotgun';
'339: Shotgun shells 1';
'340: Shotgun shells 2';
'341: Grappling gun';
'342: Grappling type 1';
'343: Grappling type 2';
'344: Grappling type 3';
'345: HK Gun';
'346: HK ammo';
'347: Revolver';
'348: Revolver bullets';
'349: Big Medipack';
'350: Small Medipack';
'351: Laser sight';
'352: Binoculars';
'353: Silencer';
'354: Burning flare';
'355: Flares';
'356: Timer';
'357: Load inventory';
'358: Save inventory';
'359: Disk load';
'360: Disk save';
'361: Memcard load';
'362: Memcard save';
'363: Smoke emitter white';
'364: Smoke emitter black';
'365: Steam emitter';
'366: Earthquake';
'367: Bubbles';
'368: Waterfall mist';
'369: Gun shell';
'370: Shotgun shell';
'371: Gun flash';
'372: Color light';
'373: Blinking light';
'374: Pulse light';
'375: Strobe light';
'376: Electrical light';
'377: Lens flare';
'378: AI guard';
'379: AI ambush';
'380: AI patrol 1';
'381: AI modify';
'382: AI follow';
'383: AI patrol 2';
'384: AI X1';
'385: AI X2';
'386: Lara start position';
'387: Lara position changer';
'388: Elevator';
'389: Raising cog';
'390: Laser';
'391: Steam laser';
'392: Floor laser';
'393: Kill all triggers';
'394: Trigger triggerer';
'395: High object 1';
'396: High object 2';
'397: Smash object 1';
'398: Smash object 2';
'399: Smash object 3';
'400: Smash object 4';
'401: Smash object 5';
'402: Smash object 6';
'403: Smash object 7';
'404: Smash object 8';
'405: Meshswap 1';
'406: Meshswap 2';
'407: Meshswap 3';
'408: Body part';
'409: Camera target';
'410: Waterfall 1';
'411: Waterfall 2';
'412: ';
'413: Waterfall 3';
'414: Waterfall 4';
'415: Waterfall 5';
'416: Animating 1';
'417: Animating 1_mip';
'418: Animating 2';
'419: Animating 2_mip';
'420: Animating 3';
'421: Animating 3_mip';
'422: Animating 4';
'423: Animating 4_mip';
'424: Animating 5';
'425: Animating 5_mip';
'426: Animating 6';
'427: Animating 6_mip';
'428: Animating 7';
'429: Animating 7_mip';
'430: Animating 8';
'431: Animating 8_mip';
'432: Animating 9';
'433: Animating 9_mip';
'434: Animating 10';
'435: Animating 10_mip';
'436: Animating 11';
'437: Animating 11_mip';
'438: Animating 12';
'439: Animating 12_mip';
'440: Animating 13';
'441: Animating 13_mip';
'442: Animating 14';
'443: Animating 14_mip';
'444: Animating 15';
'445: Animating 15_mip';
'446: Animating 16';
'447: Animating 16_mip';
'448: Floor 1 flat';
'449: Floor 2 flat';
'450: Floor 1 slope 1';
'451: Floor 2 slope 1';
'452: Floor 1 slope 2';
'453: Floor 2 slope 2';
'454: Horizon';
'455: Sky graphics';
'456: Binoculars graphics';
'457: Target graphics"';
'458: Default sprites';
'459: Misc sprites';
'460: ';
'461: ';
'462: ';
'463: ';
'464: ';
'465: ';
'466: ';
'467: ';
'468: ';
'469: ';
'470: ';
'471: ';
'472: ';
'473: ';
'474: ';
'475: ';
'476: ';
'477: ';
'478: ';
'479: ';
'480: ';
'481: ';
'482: ';
'483: ';
'484: TR3 Tiger';
	}					   --needs add
	LIVENT = 0x0A2DD4
	NUMOFENT = 0			--needs add
	OFFSET_FIRST_ENTITY = 0		--needs add
	OFFSET_FIRST_ENTITY_ID = 0		 --needs add
	OFFSET_FIRST_ENTITY_X = 0		  --needs add
	OFFSET_FIRST_ENTITY_Y = 0		  --needs add
	OFFSET_FIRST_ENTITY_Z = 0		  --needs add
	OFFSET_FIRST_ENTITY_ROOM = 0		--needs add
	SIZOFENT = 0			--needs add

--QWOP:
	OFFSET_QWOP = 0x84

--Camera:
	CAMERAX = 0			 --needs add
	CAMERAY = 0			 --needs add
	CAMERAZ = 0			 --needs add
	CAMERAROOM = 0		  --needs add
	CAM_ANGLE = 0		   --needs add
	
	CAMUPFUN = 0			--needs add

--Other:
	OFFSET_ACT = 0		  --needs add
	OFFSET_TRIGGER = 0		--needs add
	SPRINT = 0x5
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------ERRORS-----------------------------------------------------------------------------------------------------------
if POINTER_LARA==0 then
	print("ROM Hash : "..HASH)
	print("Game is not in the database")
	break
end
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
while true do

X = 0
Y = 0
Z = 0
ANGLE = 0
HEALTH = 0
X_SPD = 0
Y_SPD = 0
Z_SPD = 0
ANGLE_SPD = 0
DIST = 0
TIMER2 = 0
ENTITY = 0
COUNT = 301
NAME = ''
HP = 0
TEMP = 0

while true do

-------------------------------------------------------------------------------------Basic Informations----------------------------------------------------------------------------------------------------
--Base pointer
	BASE = memory.read_u32_le( POINTER_LARA ) - OFFSET_FIX
	
	if (BASE<=0 or BASE>=0x200000) then
		emu.frameadvance()
		--print(string.format("%x", BASE+0x80000000))
		--print(string.format("%x", POINTER_LARA ))
		--print(memory.read_u32_le( POINTER_LARA ))
		--print(BASE+0x80000000)
		break
	end
	
	if (BASE~=BASE_OLD) then
		BASE_OLD=BASE
		print("Current base address : "..string.format("%06X", BASE))
	end
	
--Changes
	X_ANC = X
	Y_ANC = Y
	Z_ANC = Z
	ANGLE_ANC = ANGLE
	TIMER_ANC = TIMER2
	
--Position of Lara
	X = memory.read_s32_le(BASE + OFFSET_POSITION)
	Y = memory.read_s32_le(BASE + OFFSET_POSITION + 0x4)
	Z = memory.read_s32_le(BASE + OFFSET_POSITION + 0x8)
	
	gui.text(50,  60, string.format("x'  (X) = %.0f:%04d (%d) {%d} [0x%X]",  X/1024.0,  X % 1024,  X,  X_SPD, BASE + OFFSET_POSITION), "white")
	gui.text(50,  80, string.format("y'  (Z) = %.0f:%04d (%d) {%d} [0x%X]",  Z/1024.0,  Z % 1024,  Z,  Z_SPD, BASE + OFFSET_POSITION + 0x8), "white")
	gui.text(50, 100, string.format("z' (-Y) = %.0f:%04d (%d) {%d} [0x%X]", -Y/1024.0, -Y % 1024, -Y, -Y_SPD, BASE + OFFSET_POSITION + 0x4), "white")
	
--Angle
	ANGLE = memory.read_u16_le(BASE + OFFSET_ANGLE)
	ANGLEDEG = ANGLE/65536*360
	gui.text(50,120, string.format("Angle = %.3f (%d) {%d} [0x%X]", ANGLEDEG, ANGLE, ANGLE_SPD, BASE + OFFSET_ANGLE), "white")

--Speed
	SPEEDH = memory.read_s16_le(BASE + OFFSET_SPEEDH)
	SPEEDV = memory.read_s16_le(BASE + OFFSET_SPEEDV)
	gui.text(50, 140, string.format("Speed horizontal = %d (real: %.2f) [0x%X]", SPEEDH, DIST, BASE + OFFSET_SPEEDH), "white")
	gui.text(50, 160, string.format("Speed vertical = %d [0x%X]", -SPEEDV, BASE + OFFSET_SPEEDV), "white")
	
--Health
	HEALTH = memory.read_s16_le(BASE + OFFSET_HEALTH)
	gui.text(50, 200, string.format("Health = %.1f%% (%d) [0x%X]", HEALTH/10.0, HEALTH, BASE + OFFSET_HEALTH), "white")
	
--QWOP
	gui.text(50, 220, string.format("QWOP = [0x%X]", BASE + OFFSET_QWOP), "white")
	
--Current room
	CURROOM1 = memory.read_u16_le(BASE + OFFSET_ROOM)
	gui.text(50, 320, string.format("Current room = %d [0x%X]", CURROOM1, BASE + OFFSET_ROOM), "white")
	
--Number of rooms in level
	NUMBERR = memory.read_u16_le(NUMROOM)
	gui.text(50, 340, string.format("Number of rooms = %d", NUMBERR), "white")

--Changes
	if ((X_ANC~=X) or (Y_ANC~=Y) or (ANGLE_ANC~=ANGLE) or (TIMER_ANC~=TIMER2)) then
		X_SPD=X-X_ANC
		Y_SPD=Y-Y_ANC
		Z_SPD=Z-Z_ANC
		ANGLE_SPD=ANGLE-ANGLE_ANC
		DIST=math.sqrt((X-X_ANC)^2+(Y-Y_ANC)^2)
	end
	
--Time
	TIMER2 = math.max(memory.read_u32_le( OFFSET_TIMER ), 0)
	MINUTES = math.floor(TIMER2/1800)
	SECONDS = math.floor(TIMER2/30)-MINUTES*60
	CENTS = math.floor((TIMER2 % 30)*(1000/30.0) + 0.5)
	
	gui.text(50, 260, string.format("Level Timer = %02d:%02d:%03d (%d)", MINUTES, SECONDS, CENTS, TIMER2), "white")
	
--TR1, TR2, TR3 Total time
	if (OFFSET_TIMER1~=0) then
		TOTAL = 0
		for i=0, TIMERLEV do
			TOTAL = TOTAL + memory.read_u16_le( OFFSET_TIMER1 + i*0x34 )
		end
		
		MINUTES=math.floor(TOTAL/1800)
		SECONDS=math.floor(TOTAL/30)-MINUTES*60
		CENTS=math.floor((TOTAL % 30)/30*100+0.5)
		
		gui.text(50,280,
		"Total Time : " .. string.format("%1d", MINUTES) .. 
		":" .. string.format("%02d", SECONDS) .. 
		"." .. string.format("%02d", CENTS) .. 
		" / " .. string.format("%5d", TOTAL),
		"white")
	end
	
--FLIPMAP
	if FLIPMAP ~= 0 then
	FLIP = memory.read_u8( FLIPMAP )
	
	if FLIP == 1
		then FLIP = "YES"
	end
	
	if FLIP == 0
		then FLIP = "NO"
	end
	
	gui.text(50,40,"Flipmap is on : "..FLIP,"white")
	end
	
--Ordinal number of current level
	if ORLEV ~= 0 then
	ORDLEV = memory.read_u8( ORLEV )
	end
-----------------------------------------------------------------------------------------------Entities----------------------------------------------------------------------------------------------------

--Entity
	TEMP = memory.read_u32_le( OFFSET_ENTITY ) - OFFSET_FIX
	
	if (TEMP>0) 
		then 	ENTITY = TEMP
				COUNT = 0
		else 	COUNT = COUNT + 1
	end
	
	NAME = ENTITY_LIST[1 + memory.read_u16_le( ENTITY + OFFSET_ENTITY_ID )]
	HP = memory.read_s16_le( ENTITY + OFFSET_ENTITY_HP )
	if HP < -100
		then HP=0 
	end
	
	if (COUNT<1) then
	gui.text(320,460," (" .. string.format("%X",( ENTITY + OFFSET_ENTITY_ID )).. ")", "white")
		if (NAME=='' or NAME==nil)
			then gui.text(50, 460, "ID: " .. string.format("%02X", memory.read_u16_le( ENTITY + OFFSET_ENTITY_ID )), "white")
			else gui.text(50, 460, NAME, "white")
		end
		gui.text(50, 480, "HP: " .. string.format("%2d", HP, "white"))
	end
	
--Living entities
	if LIVENT ~= 0 then 
	LIVENTS = memory.read_s16_le( LIVENT )
	gui.text(50,400,"Living entities : " .. LIVENTS,"white")
	gui.text(250,400,"(Shows MAX 5)","white")
	end
	
--Number of entities and list of entities in level
	--if (OFFSET_FIRST_ENTITY ~= 0 and NUMOFENT ~= 0 and OFFSET_FIRST_ENTITY_ID ~= 0 and SIZOFENT ~= 0) then
	--ENTNUM = memory.read_u16_le ( NUMOFENT )
	--BEGINOFFIENTID = memory.read_u32_le (OFFSET_FIRST_ENTITY) + OFFSET_FIRST_ENTITY_ID - OFFSET_FIX
	--FIENTX = memory.read_u32_le (OFFSET_FIRST_ENTITY) + OFFSET_FIRST_ENTITY_X - OFFSET_FIX
	--FIENTY = memory.read_u32_le (OFFSET_FIRST_ENTITY) + OFFSET_FIRST_ENTITY_Y - OFFSET_FIX
	--FIENTZ = memory.read_u32_le (OFFSET_FIRST_ENTITY) + OFFSET_FIRST_ENTITY_Z - OFFSET_FIX
	--ROENT = memory.read_u32_le (OFFSET_FIRST_ENTITY) + OFFSET_FIRST_ENTITY_ROOM - OFFSET_FIX
	--if (BEGINOFFIENTID ~= BEGINENTOLD or ENTNUM ~= ENTNUMOLD) then
	--o = 0
	--if ( ENTNUM ~= ENTNUMOLD ) then
	--print("Number of entities: " ..ENTNUM )
	--end
	--while ( o < ENTNUM ) do
	--ENTNAME = ENTITY_LIST[1 + memory.read_u16_le ( BEGINOFFIENTID + o*SIZOFENT)]
	--ENTX = memory.read_s32_le (FIENTX + o*SIZOFENT)
	--ENTY = memory.read_s32_le (FIENTY + o*SIZOFENT)
	--ENTZ = memory.read_s32_le (FIENTZ + o*SIZOFENT)
	--ROOMENT = memory.read_u16_le (ROENT + o*SIZOFENT)
	--print(ENTNAME)
	--print("X: " ..ENTX,"Y: " .. ENTY,"Z: " ..ENTZ,"Room: " ..ROOMENT)
	--o = o + 1
	--end
	--BEGINENTOLD = BEGINOFFIENTID
	--ENTNUMOLD = ENTNUM
	--end
	--end

-----------------------------------------------------------------------------------------------Camera------------------------------------------------------------------------------------------------------
	gui.text(50,520,"Camera: ", "white")
--Position of camera
	if CAMERAX ~= 0 then
	CAMX = memory.read_s32_le( CAMERAX )
	gui.text(50,540,"X Camera : " ..CAMX, "white")
	gui.text(250,540," (" .. string.format("%X",(CAMERAX)).. ")", "white")
	end
	if CAMERAY ~= 0 then
	CAMY = memory.read_s32_le( CAMERAY )
	gui.text(50,560,"Y Camera : " ..CAMY, "white")
	gui.text(250,560," (" .. string.format("%X",(CAMERAY)).. ")", "white")
	end
	if CAMERAZ ~= 0 then
	CAMZ = memory.read_s32_le( CAMERAZ )
	gui.text(50,580,"Z Camera : " ..CAMZ, "white")
	gui.text(250,580," (" .. string.format("%X",(CAMERAZ)).. ")", "white")
	end

--Camera update function
	if CAMUPFUN ~= 0 then
	gui.text(50,660,"Update function: (".. string.format("%X",(CAMUPFUN)).. ")", "white")
	end

--Room of camera
	if CAMERAROOM ~= 0 then
	CAMROOM = memory.read_u16_le ( CAMERAROOM )
	gui.text(50,600,"Camera room: " ..CAMROOM, "white")
	gui.text(250,600," (" .. string.format("%X",(CAMERAROOM)).. ")", "white")
	end
	
--Angle of camera
	if CAM_ANGLE ~= 0 then
	CAMAN = memory.read_s32_le(CAM_ANGLE)/65536*360
	gui.text(50,620,
	"Camera angle: " .. string.format("%6.2f", CAMAN) .. 
	"  / " .. string.format("%5d",CAMAN*65536/360) .. 
	" (".. string.format("%X",(CAM_ANGLE)).. ")", "white")
	end

-----------------------------------------------------------------------------------------------Other-------------------------------------------------------------------------------------------------------

--Pointer on some entity (IDK)
	POINTEONSM = memory.read_u32_le ( OFFSET_ACT )
	if OFFSET_ACT~=0
	then gui.text(50,380,"Pointer on entity (?): " .. POINTEONSM,"white") --It's pointing on something.....
	gui.text(320,380," (" .. string.format("%X",(OFFSET_ACT)).. ")", "white")
	end
	

--Extended sprint function (This function doesn't contain longest extended sprint. It's designed to Lara never stops.) -----------------------needs add
	if SPRINT ~= 0x0 then
	HOTKEYTOENABLE = {"J"}
	DETECTHOTKEY = input.get()
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--For TR3 --(Every level has different animFrame values for extended sprint)
	if SPRINT == 0x3 then
	for i, v in ipairs(HOTKEYTOENABLE) do
	if DETECTHOTKEY[v] == true then
	currentAction = memory.read_u16_le ( BASE + OFFSET_CUR_ACT )
	animFrame = memory.read_u8 ( BASE + OFFSET_ANIM_FRA )
	
--0. Lara's Home
	if ORDLEV == 0x0 then	   
	if currentAction ~= 73 or animFrame == 131 or animFrame == 138 or animFrame == 139 or animFrame == 140 or animFrame == 148 or animFrame == 174 then
	joypad.set({Up=true, R2=true}, 1)
	end
	end
	end
	end
	end
	
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
--For TR4 --(Every level has different animFrame values for extended sprint)
	if SPRINT == 0x4 then
	for i, v in ipairs(HOTKEYTOENABLE) do
	if DETECTHOTKEY[v] == true then
	currentAction = memory.read_u16_le ( BASE + OFFSET_CUR_ACT )
	animFrame = memory.read_u8 ( BASE + OFFSET_ANIM_FRA )
	
--26. Citadel Gate
	if ORDLEV == 0x1A then	   
	if currentAction ~= 73 or animFrame == 0 or animFrame == 8 or animFrame == 9 then
	joypad.set({Up=true, R2=true}, 1)
	end
	end
	end
	end
	end
	
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--For TR5 --(Every level has different animFrame values for extended sprint)
	if SPRINT == 0x5 then
	for i, v in ipairs(HOTKEYTOENABLE) do
	if DETECTHOTKEY[v] == true then
	currentAction = memory.read_u16_le ( BASE + OFFSET_CUR_ACT )
	animFrame = memory.read_u8 ( BASE + OFFSET_ANIM_FRA )
	
--1. Streets of Rome
	if ORDLEV == 0x1 then	   
	if currentAction ~= 73 or animFrame == 65 or animFrame == 73 or animFrame == 74 then
	joypad.set({Up=true, R2=true}, 1)
	end
	end
	end
	end
	end
	
end
	
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	emu.frameadvance()
	
end
end
end


